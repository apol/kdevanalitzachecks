#!/usr/bin/python2

from xml.dom.minidom import parse
import sys
import getopt
import settings

values=['int', 'double', 'float', 'uint', 'unsigned long long', 'unsigned int']
enumerations=[]

def retrieveClassElement(doc, classname):
	elems = doc.getElementsByTagName('Class')
	
	for elem in elems:
		#print elem.attributes['fullname'].value, classname
		if elem.attributes['fullname'].value.endswith(classname):
			return elem

def retrieveNSElement(doc, nsname):
	elems = doc.getElementsByTagName('Namespace')
	
	for elem in elems:
		#print >> header, elem.attributes['fullname'].value, elem.attributes.keys())
		if elem.attributes['name'].value.endswith(nsname):
			return elem

def typeToAnalitza(typename, name):
	typename=convTypename(typename)
	
	if typename=='unsigned long long': #FIXME: make it work in analitza's Cn()
		ret='Expression(Cn((uint) %s))' % name
	elif typename in values or typename=='bool' or typename in enumerations:
		ret='Expression(Cn(%s))' % name
	elif typename in [ 'QString', 'QByteArray' ]:
		ret='Expression::constructString(%s)' % name
	elif typename.startswith('QList<') and not typename.endswith('*'):
		ret='Expression::constructList(toList(%s))' % name
	elif typename.startswith('QVector<') and not typename.endswith('*'):
		ret='Expression::constructList(toList(%s.toList()))' % name
	else:
		ret='Expression::constructCustomObject(qVariantFromValue(%s),0)' % (name)
	return ret

def typenameToType(name):
	name=convTypename(name)
	
	if name in values or name in enumerations:
		return 'ExpressionType::Value'
	elif name=='bool':
		return 'ExpressionType::Bool'
	else:
		name=name.replace(" ", "")
		if name.startswith('QList<'):
			return "ExpressionType(ExpressionType::List, %s)" % typenameToType(name[6:-1])
		elif name.startswith('QVector<'):
			return "ExpressionType(ExpressionType::List, %s)" % typenameToType(name[8:-1])
		elif name in [ 'QString', 'QByteArray' ]:
			return "ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))"
		else:
			return "ExpressionType(\"%s\")" % name

def fixTypeName(name):
	name=name.replace('KDevelop::KDevelop', 'KDevelop')
	name=name.replace('KTextEditor::KDevelop', 'KDevelop')
	name=name.replace('KTextEditor::Helpers', 'Helpers')
	name=name.replace('KDevelop::KUrl', 'KUrl')
	
	if name in settings.substitutions:
		name=settings.substitutions[name]
		
	return name

def convTypename(name):
	if 'const' in name and '*' in name:
		name=name.replace("const", "")
	elif '&' in name:
		name=name.replace("const", "")
		name=name.replace("&", "")
	
	name=fixTypeName(name.strip())
	return name

def extractValue(i, argtypename):
	argtypename=argtypename.strip()
	typename=convTypename(argtypename);
	ret = '\t\t%s arg%d=' % (typename, i)
	val = 'args[%d]' % i
	
	if argtypename=='bool':
		ret+=val+'.toReal().isTrue()'
	elif argtypename in ['uint', 'int', 'unsigned int']:
		ret+=val+'.toReal().intValue()'
	elif argtypename in ['double', 'float']:
		ret+=val+'.toReal().value()'
	elif argtypename in ['QString', 'QByteArray']:
		ret+=val+'.stringValue()'
	elif argtypename.startswith("QList<"):
		ret+='fromList<%s >(%s.toExpressionList())' % (typename[6:typename.rfind('>')], val)
	else:
		argtypename=argtypename.replace(' ','')
		ret+=val+'.customObjectValue().value<%s >()' % argtypename
	return ret

def isPointer(a):
	return a[-1]=='*' or a.startswith("TypePtr<")

if __name__ == "__main__":
	try:
		opts, args = getopt.getopt(sys.argv[1:], '', ["exclude=", "type="])
		#print opts, args
	except getopt.GetoptError, err:
		# print help information and exit:
		print >> sys.stderr, "error", err
		sys.exit(2)
	
	filename=args[-1]
	classnames=args[:-1]
	
	header = open(filename+'.h', 'w')
	cpp = open(filename+'.cpp', 'w')
	
	exclude = []
	for option, value in opts:
		if option=='--exclude':
			exclude.append(value)
		elif option=='--type':
			ins=0
			ns=-1
			while ins>=0:
				ns=ins
				ins=value.find('::', ins+1)
			funcnamevalue = value[ns+2:]
			
			print >> cpp, "\tbuiltin->insertFunction(\"ref%s\"," % (funcnamevalue)
			print >> cpp, "\t\tExpressionType(ExpressionType::Lambda).addParameter(ExpressionType(\"%s\")).addParameter(ExpressionType(\"%s*\"))," % (value, value)
			print >> cpp, "\t\tnew RefQVariantType<%s>);" % (value)
	
	doc = parse(sys.stdin)
	allowed = ["Function", "FunctionDefinition", "Variable" ]
	
	addedFunctions=[]
	
	for classname in classnames:
		#we get the node for the class
		element = retrieveClassElement(doc, classname)
		
		if element:
			strclassname=classname.replace('::', '_')
			
			if element.hasAttribute('bases'):
				bases=element.attributes['bases'].value.split(';')
				wrongbases=["KShared", "TypeShared"]
				bases=filter(lambda base: len(base)>0 and base not in wrongbases, bases)
				
				for base in bases:
					base=fixTypeName(base)
					strbase=base.replace('::', '_')
					casttype    ="ExpressionType(ExpressionType::Lambda).addParameter(%s).addParameter(%s)" % (typenameToType(classname+'*'), typenameToType(base+'*'))
					downcasttype="ExpressionType(ExpressionType::Lambda).addParameter(%s).addParameter(%s)" % (typenameToType(base+'*'), typenameToType(classname+'*'))
					cancasttype ="ExpressionType(ExpressionType::Lambda).addParameter(%s).addParameter(ExpressionType::Bool)" % (typenameToType(base+'*'))
					print >> cpp, "\tbuiltin->insertFunction(\"%sTo%s\", %s, new Cast<%s, %s>);" % (strclassname, strbase, casttype, classname, base)
					print >> cpp, "\tbuiltin->insertFunction(\"%sTo%s\", %s, new DownCast<%s, %s>);" % (strbase, strclassname, downcasttype, base, classname)
					print >> cpp, "\tbuiltin->insertFunction(\"%sIs%s\", %s, new CanCast<%s, %s>);"  % (strbase, strclassname,  cancasttype, base, classname)
		else:
			elementNS = retrieveNSElement(doc, classname)

		if element:
			print >> header, "//Generating %s\n" % classname
			
			for child in element.childNodes:
				if child.nodeType!=3 and child.tagName in allowed and child.attributes['type_name'].value!='void':
					fullname=child.attributes['fullname'].value
					fullname=fixTypeName(fullname)
					
					shortname=child.attributes['name'].value
					type_name=fixTypeName(child.attributes['type_name'].value)
					isPtr=isPointer(type_name)
					aconstant='constant' in child.attributes.keys()
					
					if(shortname.startswith('operator') or shortname.startswith('~') or shortname=="d_func"
						or fullname==classname or child.attributes['access'].value!='public'):
						continue
					
					memberof=fixTypeName(child.attributes['member_of'].value)
					compname=fullname.replace('::', '_')
					
					#print '//pepepe', memberof, shortname, exclude, compname
					if memberof.endswith('::'+shortname) or (not aconstant and child.tagName=='Function') or type_name in exclude or compname.endswith('Internal'):
						continue
					
					if child.hasAttribute('member_template_parameters'):
						continue
					
					if compname in addedFunctions:
						print "Skipping overloaded function ", fullname
						continue
					
					addedFunctions.append(compname)
					theclassname=classname+'*'
					
					if theclassname in settings.substitutions:
						theclassname=settings.substitutions[theclassname]
					
					print >> header, "struct %s : public Analitza::FunctionDefinition\n{" % compname
					print >> header, '\tvirtual Expression operator()(const QList<Expression>& args)\n\t{'
					print >> header, '\t\t%s thisElement=args[0].customObjectValue().value<%s >();' % (theclassname, theclassname)
					print >> header, '\t\tQ_ASSERT(thisElement && "calling %s");' % fullname
					
					print >> cpp, "\tbuiltin->insertFunction(\"%s\", %s::type(), new %s);" % (compname, compname, compname)
					
					analitzatype='static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(%s)' % typenameToType(theclassname)
					if child.tagName=='Variable':
						print >> header, '\t\treturn %s;' % typeToAnalitza(type_name, 'thisElement->'+shortname)
					elif child.tagName in ['Function', 'FunctionDefinition']:
						i=1
						args=[]
						for arg in child.childNodes:
							if arg.nodeType!=3:
								argtypename=convTypename(arg.attributes['type_name'].value)
								
								print >> header, extractValue(i, argtypename)+';'
								args.append('arg%d' % i)
								analitzatype+='.addParameter(%s)' % typenameToType(argtypename)
								
								i+=1
						print >> header, '\t\t%s theRet(%s);' % (type_name, 'thisElement->'+shortname+'('+', '.join(args)+')')
						if isPtr:
							print >> header, '\t\treturn theRet ? %s : Expression::constructCustomObject(QVariant(), 0);' % typeToAnalitza(type_name, 'theRet')
						else:
							print >> header, '\t\treturn %s;' % typeToAnalitza(type_name, 'theRet')
					else:
						print >> sys.stderr, "Error:", child.tagName, fullname, child.attributes.keys()
					
					analitzatype+='.addParameter(%s)' % typenameToType(type_name)
					
					print >> header, "\t}\n"
					print >> header, "\t%s; }" % analitzatype
					print >> header, "};\n"
				elif child.nodeType==child.ELEMENT_NODE and child.tagName=='Enum':
					name = fixTypeName(child.attributes['fullname'].value)
					enumerations.append(name)
					
					for enumeratorValue in child.childNodes:
						if enumeratorValue.nodeType==child.ELEMENT_NODE:
							name = enumeratorValue.attributes['fullname'].value
							strname=name.replace('::','_')
							value = enumeratorValue.attributes['value'].value
							print >> cpp, "\tvariables->modify(\"%s\", Cn(%s));" % (strname, name)
		elif elementNS:
			
			for child in elementNS.childNodes:
				if child.nodeType!=3 and child.tagName in allowed and child.attributes['type_name'].value!='void':
					fullname=fixTypeName(child.attributes['fullname'].value)
					
					shortname=child.attributes['name'].value
					type_name=fixTypeName(child.attributes['type_name'].value)
					aconstant='constant' in child.attributes.keys()
					
					compname=fullname.replace('::', '_')
					
					if compname in addedFunctions:
						print "Skipping overloaded function ", fullname
						continue
					
					addedFunctions.append(compname)
					
					print >> header, "struct %s : public Analitza::FunctionDefinition\n{" % compname
					print >> header, "\tvirtual Expression operator()(const QList<Expression>& args)\n\t{"
					
					print >> cpp, "\tbuiltin->insertFunction(\"%s\", %s::type(), new %s);" % (shortname, compname, compname)
					
					analitzatype='static ExpressionType type() { return ExpressionType(ExpressionType::Lambda)'
					if child.tagName=='Variable':
						print >> header, '\t\tQ_UNUSED(args)'
						print >> header, '\t\treturn %s;' % typeToAnalitza(type_name, shortname)
					elif child.tagName=='Function':
						i=0
						args=[]
						for arg in child.childNodes:
							if arg.nodeType!=3:
								argtypename=convTypename(arg.attributes['type_name'].value)
								
								print >> header, extractValue(i, argtypename)+';'
								args.append('arg%d' % i)
								analitzatype+='.addParameter(%s)' % typenameToType(argtypename)
								i+=1
						if i==0:
							print >> header, '\t\tQ_UNUSED(args)'
							
						print >> header, '\t\t%s theRet = %s;' % (type_name, classname+'::'+shortname+'('+', '.join(args)+')')
						print >> header, '\t\treturn %s;' % typeToAnalitza(type_name, 'theRet')
					else:
						print >> sys.stderr, "Error:", child.tagName, fullname, child.attributes.keys()
					
					analitzatype+='.addParameter(%s)' % typenameToType(type_name)
					
					print >> header, "\t}\n"
					print >> header, "\t%s; }" % analitzatype
					print >> header, "};\n"
		else:
			print >> sys.stderr, "Class not found"
			sys.exit(2)
