
#include <analitza/expression.h>
#include <analitza/builtinmethods.h>
#include <analitza/value.h>

#include <interfaces/iassistant.h>
#include <interfaces/ilanguagecheck.h>
#include <language/interfaces/iproblem.h>
#include <language/interfaces/iastcontainer.h>
#include <language/duchain/declaration.h>
#include <language/duchain/use.h>
#include <language/duchain/forwarddeclaration.h>
#include <language/duchain/ducontext.h>
#include <language/duchain/parsingenvironment.h>
#include <language/duchain/classmemberdeclaration.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/functiondefinition.h>
#include <language/checks/controlflowgraph.h>
#include <language/checks/controlflownode.h>
#include <language/checks/dataaccessrepository.h>
#include "helpers.h"


using Analitza::Cn;
using Analitza::Expression;
using Analitza::ExpressionType;
using namespace KDevelop;

Q_DECLARE_METATYPE(const KDevelop::Declaration*);
Q_DECLARE_METATYPE(KDevelop::Declaration*);
Q_DECLARE_METATYPE(KDevelop::DeclarationId);
Q_DECLARE_METATYPE(KDevelop::Declaration::Kind);
Q_DECLARE_METATYPE(KDevelop::Declaration::AccessPolicy);
Q_DECLARE_METATYPE(KDevelop::Use*);
Q_DECLARE_METATYPE(const KDevelop::Use*);
Q_DECLARE_METATYPE(KDevelop::DUContext*);
Q_DECLARE_METATYPE(KDevelop::DUContext::SearchFlags);
Q_DECLARE_METATYPE(KDevelop::TopDUContext*);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::Flags);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::Features);
Q_DECLARE_METATYPE(KDevelop::ParsingEnvironmentFilePointer);
Q_DECLARE_METATYPE(KDevelop::ForwardDeclaration*);
Q_DECLARE_METATYPE(KDevelop::AbstractFunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::FunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::FunctionDefinition*);
Q_DECLARE_METATYPE(KDevelop::ClassFunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::ClassMemberDeclaration*);
Q_DECLARE_METATYPE(const KDevelop::ForwardDeclaration*);
Q_DECLARE_METATYPE(KDevelop::QualifiedIdentifier*);
Q_DECLARE_METATYPE(KDevelop::IndexedType);
Q_DECLARE_METATYPE(KDevelop::IndexedInstantiationInformation);
Q_DECLARE_METATYPE(KDevelop::DocumentRange);
Q_DECLARE_METATYPE(KDevelop::DocumentCursor);
Q_DECLARE_METATYPE(QStack<KDevelop::DocumentCursor>);
Q_DECLARE_METATYPE(KDevelop::IAssistant::Ptr);
Q_DECLARE_METATYPE(KDevelop::Problem*);
Q_DECLARE_METATYPE(KDevelop::ProblemPointer);
Q_DECLARE_METATYPE(KDevelop::ProblemData::Source);
Q_DECLARE_METATYPE(KDevelop::ProblemData::Severity);
Q_DECLARE_METATYPE(KDevelop::CheckData*);
Q_DECLARE_METATYPE(KDevelop::DUContext::Import);
Q_DECLARE_METATYPE(KDevelop::DUContext::Import*);
Q_DECLARE_METATYPE(KDevelop::IndexedDUContext);
Q_DECLARE_METATYPE(KDevelop::IndexedDUContext*);
Q_DECLARE_METATYPE(KDevVarLengthArray<IndexedDUContext>);
Q_DECLARE_METATYPE(KDevelop::DUContext::ContextType);
Q_DECLARE_METATYPE(KDevVarLengthArray<Declaration*>);
Q_DECLARE_METATYPE(KDevelop::ControlFlowNode*);
Q_DECLARE_METATYPE(KDevelop::ControlFlowNode::Type);
Q_DECLARE_METATYPE(KDevelop::ControlFlowGraph*);
Q_DECLARE_METATYPE(KDevelop::DataAccess*);
Q_DECLARE_METATYPE(KDevelop::DataAccessRepository*);
Q_DECLARE_METATYPE(KDevelop::SimpleRange);
Q_DECLARE_METATYPE(KDevelop::DUChainBase*);
Q_DECLARE_METATYPE(KDevelop::DUChainBaseData*);
Q_DECLARE_METATYPE(const KDevelop::DUChainBaseData*);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::DUChainPointerData>);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::PersistentMovingRange>);
Q_DECLARE_METATYPE(KDevelop::AbstractType::Ptr);
Q_DECLARE_METATYPE(KDevelop::AbstractType*);
Q_DECLARE_METATYPE(KDevelop::AbstractType::WhichType);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::IAstContainer>);
Q_DECLARE_METATYPE(QSharedData*);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::IndexedRecursiveImports);
Q_DECLARE_METATYPE(const KDevelop::IndexedString*);
Q_DECLARE_METATYPE(KDevelop::DataAccess::DataAccessFlags);

typedef QPair<KDevelop::Declaration*,int> __kk1; Q_DECLARE_METATYPE(__kk1);
typedef QPair<uint,uint> __kk2; Q_DECLARE_METATYPE(__kk2);
Q_DECLARE_METATYPE(KDevVarLengthArray<KDevelop::DUContext::SearchItem::Ptr>);

typedef QMap<IndexedString,QList<SimpleRange> > __idxstringtosimpleranges;
Q_DECLARE_METATYPE(__idxstringtosimpleranges);

typedef QMap<IndexedString,QList<RangeInRevision> > __idxstringtorevranges;
Q_DECLARE_METATYPE(__idxstringtorevranges);

typedef KDevelop::MergeAbstractFunctionDeclaration<KDevelop::ClassMemberDeclaration, KDevelop::ClassFunctionDeclarationData> __mergeClassMemberClassFunctionDecls;
Q_DECLARE_METATYPE(__mergeClassMemberClassFunctionDecls*);

template <class T>
QList<Expression> toList(const QList< T >& returned);

template <class T>
QList<T> fromList(const QList<Expression>& exps);

#define SearchFlags KDevelop::DUContext::SearchFlags
#define AccessPolicy KDevelop::Declaration::AccessPolicy
