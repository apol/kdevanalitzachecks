/* This file is part of KDevelop
    Copyright 2011 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "analitzacheck.h"
#include <analitza/expression.h>
#include <analitza/analyzer.h>
#include "bindings.h"
#include <KPluginInfo>
#include <KStandardDirs>
#include <language/interfaces/iproblem.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>

using namespace Analitza;

int AnalitzaCheck::area=0;

QStringList AnalitzaCheck::listChecks()
{
	return KGlobal::dirs()->findAllResources("data", "kdevanalitzachecks/*.kdevcheck");
}

AnalitzaCheck::AnalitzaCheck()
	: m_isSetup(false)
{
	registerFunctions(a.variables(), a.builtinMethods());
}

AnalitzaCheck::AnalitzaCheck(const QString& path)
	: m_isSetup(false)
{
	KPluginInfo::List plugins = KPluginInfo::fromFiles(QStringList(path));
	Q_ASSERT(plugins.size()==1);
	
	m_plugin = plugins.first();
	kDebug(area) << "found analitza plugin:" << path << m_plugin.name();
	
	QDir d(QFileInfo(path).dir());
	m_path=d.absoluteFilePath(m_plugin.pluginName());
}

QString AnalitzaCheck::name() const
{
	return m_plugin.name();
}

void AnalitzaCheck::create()
{
	QFile f(m_path);
// 	qDebug() << "peee" << d.absoluteFilePath(m_plugin.pluginName());
	bool b = f.open(QIODevice::ReadOnly | QFile::Text);
	Q_ASSERT(b);
	
	registerFunctions(a.variables(), a.builtinMethods());
	
	QTextStream stream(&f);
	a.importScript(&stream);
}

void AnalitzaCheck::setup()
{
	if(m_isSetup)
		return;
	
	create();
	a.setExpression(Expression("check"));
	if(!a.isCorrect())
		kDebug(area) << "analitza check error:" << name() << a.errors();
	Q_ASSERT(a.isCorrect());
	
	Analitza::ExpressionType t=a.type();
	ExpressionType testType=ExpressionType(ExpressionType::Lambda)
		.addParameter(ExpressionType("KDevelop::CheckData*"))
		.addParameter(ExpressionType(ExpressionType::List, ExpressionType("KSharedPtr<KDevelop::Problem>")));
								
	Q_ASSERT(t.canReduceTo(testType));
	
	a.setExpression(a.calculate());
	if(!a.isCorrect())
		kDebug(area) << "analitza check error:" << name() << a.errors();
	Q_ASSERT(a.isCorrect());
	
	m_isSetup = true;
}

Q_DECLARE_METATYPE(KDevelop::ProblemPointer);
Q_DECLARE_METATYPE(KDevelop::CheckData*);

void AnalitzaCheck::runCheck(const KDevelop::CheckData& data)
{
	QMutexLocker checkLock(&m_mutex);
	setup();
	
	kDebug(area) << "running analitza check" << data.url << name();
	Expression obj = Expression::constructCustomObject(QVariant::fromValue<KDevelop::CheckData*>((KDevelop::CheckData*) &data),0);
	QStack<Object*> stack;
	stack.push(obj.tree());
	a.setStack(stack);
	KDevelop::DUChainReadLocker lock(KDevelop::DUChain::lock());
	Expression result = a.calculateLambda();
	lock.unlock();
	
	QList<Expression> list = result.toExpressionList();
	kDebug(area) << "result..." << list.size() << result.toString();
	foreach(const Expression& problems, list) {
		KDevelop::DUChainWriteLocker lock(KDevelop::DUChain::lock());
		KDevelop::ProblemPointer p = problems.customObjectValue().value<KDevelop::ProblemPointer>();
		data.top->addProblem(p);
		
		kDebug(area) << "Registering problem for " << name() << p->range() << p->finalLocation();
	}
}

