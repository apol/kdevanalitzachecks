substitutions = {
	'KDevelop::AbstractType*' : 'TypePtr<AbstractType>',
	'AbstractType::Ptr' : 'TypePtr<AbstractType>',
	'DUContext*' : 'KDevelop::DUContext*',
	'Declaration*' : 'KDevelop::Declaration*',
	'DataAccess*' : 'KDevelop::DataAccess*',
	'ControlFlowNode*' : 'KDevelop::ControlFlowNode*',
	'QVector<Import>' : 'QVector<KDevelop::DUContext::Import>',
	'DataAccessFlags' : 'KDevelop::DataAccess::DataAccessFlags',
	'CursorInRevision' : 'KDevelop::CursorInRevision',
	'RangeInRevision' : 'KDevelop::RangeInRevision',
	'IndexedString' : 'KDevelop::IndexedString',
	'Identifier' : 'KDevelop::Identifier'
}