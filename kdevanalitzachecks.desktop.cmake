[Desktop Entry]
Type=Service
Name=KDevAnalitzaChecks
GenericName=KDevelop Analitza Checks
Comment=Helps you define static checks against your code.
ServiceTypes=KDevelop/Plugin
X-KDE-Library=kdevanalitzachecksplugin
X-KDE-PluginInfo-Name=kdevanalitzachecks
X-KDE-PluginInfo-Author=Aleix Pol
X-KDE-PluginInfo-Email=aleixpol@kde.org
X-KDE-PluginInfo-License=GPL
X-KDevelop-Version=@KDEV_PLUGIN_VERSION@
X-KDevelop-Category=Global
X-KDevelop-Mode=NoGUI
X-KDevelop-Interfaces=org.kdevelop.ILanguageCheckProvider
