
#include <analitza/expression.h>
#include <analitza/builtinmethods.h>
#include <analitza/value.h>

#include <interfaces/iassistant.h>
#include <interfaces/ilanguagecheck.h>
#include <language/interfaces/iproblem.h>
#include <language/interfaces/iastcontainer.h>
#include <language/duchain/declaration.h>
#include <language/duchain/use.h>
#include <language/duchain/forwarddeclaration.h>
#include <language/duchain/ducontext.h>
#include <language/duchain/parsingenvironment.h>
#include <language/duchain/classmemberdeclaration.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/duchain/functiondefinition.h>
#include <language/checks/controlflowgraph.h>
#include <language/checks/controlflownode.h>
#include <language/checks/dataaccessrepository.h>
#include "helpers.h"


using Analitza::Cn;
using Analitza::Expression;
using Analitza::ExpressionType;
using namespace KDevelop;

Q_DECLARE_METATYPE(const KDevelop::Declaration*);
Q_DECLARE_METATYPE(KDevelop::Declaration*);
Q_DECLARE_METATYPE(KDevelop::DeclarationId);
Q_DECLARE_METATYPE(KDevelop::Declaration::Kind);
Q_DECLARE_METATYPE(KDevelop::Declaration::AccessPolicy);
Q_DECLARE_METATYPE(KDevelop::Use*);
Q_DECLARE_METATYPE(const KDevelop::Use*);
Q_DECLARE_METATYPE(KDevelop::DUContext*);
Q_DECLARE_METATYPE(KDevelop::DUContext::SearchFlags);
Q_DECLARE_METATYPE(KDevelop::TopDUContext*);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::Flags);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::Features);
Q_DECLARE_METATYPE(KDevelop::ParsingEnvironmentFilePointer);
Q_DECLARE_METATYPE(KDevelop::ForwardDeclaration*);
Q_DECLARE_METATYPE(KDevelop::AbstractFunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::FunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::FunctionDefinition*);
Q_DECLARE_METATYPE(KDevelop::ClassFunctionDeclaration*);
Q_DECLARE_METATYPE(KDevelop::ClassMemberDeclaration*);
Q_DECLARE_METATYPE(const KDevelop::ForwardDeclaration*);
Q_DECLARE_METATYPE(KDevelop::QualifiedIdentifier*);
Q_DECLARE_METATYPE(KDevelop::IndexedType);
Q_DECLARE_METATYPE(KDevelop::IndexedInstantiationInformation);
Q_DECLARE_METATYPE(KDevelop::DocumentRange);
Q_DECLARE_METATYPE(KDevelop::DocumentCursor);
Q_DECLARE_METATYPE(QStack<KDevelop::DocumentCursor>);
Q_DECLARE_METATYPE(KDevelop::IAssistant::Ptr);
Q_DECLARE_METATYPE(KDevelop::Problem*);
Q_DECLARE_METATYPE(KDevelop::ProblemPointer);
Q_DECLARE_METATYPE(KDevelop::ProblemData::Source);
Q_DECLARE_METATYPE(KDevelop::ProblemData::Severity);
Q_DECLARE_METATYPE(KDevelop::CheckData*);
Q_DECLARE_METATYPE(KDevelop::DUContext::Import);
Q_DECLARE_METATYPE(KDevelop::DUContext::Import*);
Q_DECLARE_METATYPE(KDevelop::IndexedDUContext);
Q_DECLARE_METATYPE(KDevelop::IndexedDUContext*);
Q_DECLARE_METATYPE(KDevVarLengthArray<IndexedDUContext>);
Q_DECLARE_METATYPE(KDevelop::DUContext::ContextType);
Q_DECLARE_METATYPE(KDevVarLengthArray<Declaration*>);
Q_DECLARE_METATYPE(KDevelop::ControlFlowNode*);
Q_DECLARE_METATYPE(KDevelop::ControlFlowNode::Type);
Q_DECLARE_METATYPE(KDevelop::ControlFlowGraph*);
Q_DECLARE_METATYPE(KDevelop::DataAccess*);
Q_DECLARE_METATYPE(KDevelop::DataAccessRepository*);
Q_DECLARE_METATYPE(KDevelop::SimpleRange);
Q_DECLARE_METATYPE(KDevelop::DUChainBase*);
Q_DECLARE_METATYPE(KDevelop::DUChainBaseData*);
Q_DECLARE_METATYPE(const KDevelop::DUChainBaseData*);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::DUChainPointerData>);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::PersistentMovingRange>);
Q_DECLARE_METATYPE(KDevelop::AbstractType::Ptr);
Q_DECLARE_METATYPE(KDevelop::AbstractType*);
Q_DECLARE_METATYPE(KDevelop::AbstractType::WhichType);
Q_DECLARE_METATYPE(KSharedPtr<KDevelop::IAstContainer>);
Q_DECLARE_METATYPE(QSharedData*);
Q_DECLARE_METATYPE(KDevelop::TopDUContext::IndexedRecursiveImports);
Q_DECLARE_METATYPE(const KDevelop::IndexedString*);
Q_DECLARE_METATYPE(KDevelop::DataAccess::DataAccessFlags);

typedef QPair<KDevelop::Declaration*,int> __kk1; Q_DECLARE_METATYPE(__kk1);
typedef QPair<uint,uint> __kk2; Q_DECLARE_METATYPE(__kk2);
Q_DECLARE_METATYPE(KDevVarLengthArray<KDevelop::DUContext::SearchItem::Ptr>);

typedef QMap<IndexedString,QList<SimpleRange> > __idxstringtosimpleranges;
Q_DECLARE_METATYPE(__idxstringtosimpleranges);

typedef QMap<IndexedString,QList<RangeInRevision> > __idxstringtorevranges;
Q_DECLARE_METATYPE(__idxstringtorevranges);

typedef KDevelop::MergeAbstractFunctionDeclaration<KDevelop::ClassMemberDeclaration, KDevelop::ClassFunctionDeclarationData> __mergeClassMemberClassFunctionDecls;
Q_DECLARE_METATYPE(__mergeClassMemberClassFunctionDecls*);

template <class T>
QList<Expression> toList(const QList< T >& returned);

template <class T>
QList<T> fromList(const QList<Expression>& exps);

#define SearchFlags KDevelop::DUContext::SearchFlags
#define AccessPolicy KDevelop::Declaration::AccessPolicy
//Generating KDevelop::Declaration

struct KDevelop_Declaration_isDefinition : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isDefinition");
		bool theRet(thisElement->isDefinition());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_ownIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::ownIndex");
		uint theRet(thisElement->ownIndex());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_Declaration_equalQualifiedIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::equalQualifiedIdentifier");
		KDevelop::Declaration * arg1=args[1].customObjectValue().value<KDevelop::Declaration* >();
		bool theRet(thisElement->equalQualifiedIdentifier(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_logicalInternalContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::logicalInternalContext");
		KDevelop::TopDUContext * arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		KDevelop::DUContext* theRet(thisElement->logicalInternalContext(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_Declaration_hasUses : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::hasUses");
		bool theRet(thisElement->hasUses());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_isForwardDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isForwardDeclaration");
		bool theRet(thisElement->isForwardDeclaration());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_identifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::identifier");
		KDevelop::Identifier theRet(thisElement->identifier());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::Identifier")); }
};

struct KDevelop_Declaration_clone : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::clone");
		KDevelop::Declaration* theRet(thisElement->clone());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

struct KDevelop_Declaration_uses : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::uses");
		QMap<IndexedString,QList<RangeInRevision> > theRet(thisElement->uses());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("QMap<IndexedString,QList<RangeInRevision>>")); }
};

struct KDevelop_Declaration_indexedType : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::indexedType");
		KDevelop::IndexedType theRet(thisElement->indexedType());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::IndexedType")); }
};

struct KDevelop_Declaration_additionalIdentity : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::additionalIdentity");
		uint theRet(thisElement->additionalIdentity());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_Declaration_isExplicitlyDeleted : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isExplicitlyDeleted");
		bool theRet(thisElement->isExplicitlyDeleted());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_inDUChain : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::inDUChain");
		bool theRet(thisElement->inDUChain());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_specialization : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::specialization");
		KDevelop::IndexedInstantiationInformation theRet(thisElement->specialization());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::IndexedInstantiationInformation")); }
};

struct KDevelop_Declaration_isFinal : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isFinal");
		bool theRet(thisElement->isFinal());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_isAnonymous : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isAnonymous");
		bool theRet(thisElement->isAnonymous());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_comment : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::comment");
		QByteArray theRet(thisElement->comment());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_Declaration_internalContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::internalContext");
		KDevelop::DUContext* theRet(thisElement->internalContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_Declaration_inSymbolTable : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::inSymbolTable");
		bool theRet(thisElement->inSymbolTable());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_usesCurrentRevision : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::usesCurrentRevision");
		QMap<IndexedString,QList<SimpleRange> > theRet(thisElement->usesCurrentRevision());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("QMap<IndexedString,QList<SimpleRange>>")); }
};

struct KDevelop_Declaration_toForwardDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::toForwardDeclaration");
		KDevelop::ForwardDeclaration const* theRet(thisElement->toForwardDeclaration());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::ForwardDeclaration*")); }
};

struct KDevelop_Declaration_isAutoDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isAutoDeclaration");
		bool theRet(thisElement->isAutoDeclaration());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_abstractType : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::abstractType");
		TypePtr<AbstractType> theRet(thisElement->abstractType());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("TypePtr<AbstractType>")); }
};

struct KDevelop_Declaration_id : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::id");
		bool arg1=args[1].toReal().isTrue();
		KDevelop::DeclarationId theRet(thisElement->id(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool).addParameter(ExpressionType("KDevelop::DeclarationId")); }
};

struct KDevelop_Declaration_indexedIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::indexedIdentifier");
		IndexedIdentifier const& theRet(thisElement->indexedIdentifier());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("IndexedIdentifier")); }
};

struct KDevelop_Declaration_toString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::toString");
		QString theRet(thisElement->toString());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_Declaration_topContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::topContext");
		KDevelop::TopDUContext* theRet(thisElement->topContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct KDevelop_Declaration_alwaysForceDirect : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::alwaysForceDirect");
		bool theRet(thisElement->alwaysForceDirect());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_context : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::context");
		KDevelop::DUContext* theRet(thisElement->context());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_Declaration_logicalDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::logicalDeclaration");
		KDevelop::TopDUContext * arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		KDevelop::Declaration const* theRet(thisElement->logicalDeclaration(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

struct KDevelop_Declaration_isTypeAlias : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isTypeAlias");
		bool theRet(thisElement->isTypeAlias());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_isFunctionDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::isFunctionDeclaration");
		bool theRet(thisElement->isFunctionDeclaration());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_Declaration_qualifiedIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::qualifiedIdentifier");
		KDevelop::QualifiedIdentifier theRet(thisElement->qualifiedIdentifier());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_Declaration_kind : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* thisElement=args[0].customObjectValue().value<KDevelop::Declaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::Declaration::kind");
		KDevelop::Declaration::Kind theRet(thisElement->kind());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Value); }
};

//Generating KDevelop::Use

struct KDevelop_Use_usedDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Use* thisElement=args[0].customObjectValue().value<KDevelop::Use* >();
		Q_ASSERT(thisElement && "calling KDevelop::Use::usedDeclaration");
		KDevelop::TopDUContext* arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		KDevelop::Declaration* theRet(thisElement->usedDeclaration(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Use*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

struct KDevelop_Use_m_declarationIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Use* thisElement=args[0].customObjectValue().value<KDevelop::Use* >();
		Q_ASSERT(thisElement && "calling KDevelop::Use::m_declarationIndex");
		return Expression(Cn(thisElement->m_declarationIndex));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Use*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_Use_m_range : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Use* thisElement=args[0].customObjectValue().value<KDevelop::Use* >();
		Q_ASSERT(thisElement && "calling KDevelop::Use::m_range");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->m_range),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Use*")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

//Generating KDevelop::TopDUContext

struct KDevelop_TopDUContext_imports : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::imports");
		KDevelop::DUContext * arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::CursorInRevision arg2=args[2].customObjectValue().value<KDevelop::CursorInRevision >();
		bool theRet(thisElement->imports(arg1, arg2));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_TopDUContext_ownIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::ownIndex");
		uint theRet(thisElement->ownIndex());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_TopDUContext_sharedDataOwner : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::sharedDataOwner");
		KDevelop::TopDUContext* theRet(thisElement->sharedDataOwner());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct KDevelop_TopDUContext_parsingEnvironmentFile : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::parsingEnvironmentFile");
		KSharedPtr<ParsingEnvironmentFile> theRet(thisElement->parsingEnvironmentFile());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KSharedPtr<ParsingEnvironmentFile>")); }
};

struct KDevelop_TopDUContext_topContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::topContext");
		KDevelop::TopDUContext* theRet(thisElement->topContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct KDevelop_TopDUContext_inDUChain : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::inDUChain");
		bool theRet(thisElement->inDUChain());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_TopDUContext_indexed : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::indexed");
		KDevelop::IndexedTopDUContext theRet(thisElement->indexed());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::IndexedTopDUContext")); }
};

struct KDevelop_TopDUContext_importPosition : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::importPosition");
		KDevelop::DUContext * arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::CursorInRevision theRet(thisElement->importPosition(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct KDevelop_TopDUContext_isOnDisk : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::isOnDisk");
		bool theRet(thisElement->isOnDisk());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_TopDUContext_deleting : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::deleting");
		bool theRet(thisElement->deleting());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_TopDUContext_loadedImporters : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::loadedImporters");
		QList<DUContext*> theRet(thisElement->loadedImporters());
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext*"))); }
};

struct KDevelop_TopDUContext_flags : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::flags");
		KDevelop::TopDUContext::Flags theRet(thisElement->flags());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_TopDUContext_features : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::features");
		KDevelop::TopDUContext::Features theRet(thisElement->features());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_TopDUContext_problems : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::problems");
		QList<ProblemPointer> theRet(thisElement->problems());
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("ProblemPointer"))); }
};

struct KDevelop_TopDUContext_importedParentContexts : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::importedParentContexts");
		QVector<KDevelop::DUContext::Import> theRet(thisElement->importedParentContexts());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext::Import"))); }
};

struct KDevelop_TopDUContext_importers : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::importers");
		QVector<DUContext*> theRet(thisElement->importers());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext*"))); }
};

struct KDevelop_TopDUContext_ast : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::ast");
		KSharedPtr<IAstContainer> theRet(thisElement->ast());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KSharedPtr<IAstContainer>")); }
};

struct KDevelop_TopDUContext_usingImportsCache : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::usingImportsCache");
		bool theRet(thisElement->usingImportsCache());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_TopDUContext_recursiveImportIndices : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::recursiveImportIndices");
		Utils::StorableSet<IndexedTopDUContext,IndexedTopDUContextIndexConversion,RecursiveImportRepository,true> const& theRet(thisElement->recursiveImportIndices());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("Utils::StorableSet<IndexedTopDUContext,IndexedTopDUContextIndexConversion,RecursiveImportRepository,true>")); }
};

struct KDevelop_TopDUContext_url : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::url");
		KDevelop::IndexedString theRet(thisElement->url());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::IndexedString")); }
};

struct KDevelop_TopDUContext_usedDeclarationForIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* thisElement=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::TopDUContext::usedDeclarationForIndex");
		unsigned int arg1=args[1].toReal().intValue();
		KDevelop::Declaration* theRet(thisElement->usedDeclarationForIndex(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

//Generating KDevelop::DUContext::Import

struct KDevelop_DUContext_Import_indexedContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::indexedContext");
		KDevelop::IndexedDUContext theRet(thisElement->indexedContext());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType("KDevelop::IndexedDUContext")); }
};

struct KDevelop_DUContext_Import_isDirect : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::isDirect");
		bool theRet(thisElement->isDirect());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_Import_context : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::context");
		KDevelop::TopDUContext * arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		bool arg2=args[2].toReal().isTrue();
		KDevelop::DUContext* theRet(thisElement->context(arg1, arg2));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_DUContext_Import_topContextIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::topContextIndex");
		uint theRet(thisElement->topContextIndex());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_DUContext_Import_indirectDeclarationId : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::indirectDeclarationId");
		KDevelop::DeclarationId theRet(thisElement->indirectDeclarationId());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType("KDevelop::DeclarationId")); }
};

struct KDevelop_DUContext_Import_position : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext::Import* thisElement=args[0].customObjectValue().value<KDevelop::DUContext::Import* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::Import::position");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->position),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import*")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

//Generating KDevelop::DUContext

struct KDevelop_DUContext_isPropagateDeclarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::isPropagateDeclarations");
		bool theRet(thisElement->isPropagateDeclarations());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_localDeclarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::localDeclarations");
		KDevelop::TopDUContext * arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		QVector<Declaration*> theRet(thisElement->localDeclarations(arg1));
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Declaration*"))); }
};

struct KDevelop_DUContext_indexedImporters : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::indexedImporters");
		KDevVarLengthArray<IndexedDUContext> theRet(thisElement->indexedImporters());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevVarLengthArray<IndexedDUContext>")); }
};

struct KDevelop_DUContext_allDeclarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::allDeclarations");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::TopDUContext * arg2=args[2].customObjectValue().value<KDevelop::TopDUContext* >();
		bool arg3=args[3].toReal().isTrue();
		QList<QPair<Declaration*,int> > theRet(thisElement->allDeclarations(arg1, arg2, arg3));
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType::Bool).addParameter(ExpressionType(ExpressionType::List, ExpressionType("QPair<Declaration*,int>"))); }
};

struct KDevelop_DUContext_importPosition : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::importPosition");
		KDevelop::DUContext * arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::CursorInRevision theRet(thisElement->importPosition(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct KDevelop_DUContext_uses : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::uses");
		KDevelop::Use const* theRet(thisElement->uses());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::Use*")); }
};

struct KDevelop_DUContext_findUseAt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findUseAt");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		int theRet(thisElement->findUseAt(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Value); }
};

struct KDevelop_DUContext_depth : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::depth");
		int theRet(thisElement->depth());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_DUContext_findContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findContext");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::DUContext* arg2=args[2].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::DUContext* theRet(thisElement->findContext(arg1, arg2));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_DUContext_imports : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::imports");
		KDevelop::DUContext * arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::CursorInRevision arg2=args[2].customObjectValue().value<KDevelop::CursorInRevision >();
		bool theRet(thisElement->imports(arg1, arg2));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_inDUChain : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::inDUChain");
		bool theRet(thisElement->inDUChain());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_createNavigationWidget : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::createNavigationWidget");
		KDevelop::Declaration* arg1=args[1].customObjectValue().value<KDevelop::Declaration* >();
		KDevelop::TopDUContext* arg2=args[2].customObjectValue().value<KDevelop::TopDUContext* >();
		QString arg3=args[3].stringValue();
		QString arg4=args[4].stringValue();
		QWidget* theRet(thisElement->createNavigationWidget(arg1, arg2, arg3, arg4));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))).addParameter(ExpressionType("QWidget*")); }
};

struct KDevelop_DUContext_owner : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::owner");
		KDevelop::Declaration* theRet(thisElement->owner());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

struct KDevelop_DUContext_inSymbolTable : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::inSymbolTable");
		bool theRet(thisElement->inSymbolTable());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_indexedLocalScopeIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::indexedLocalScopeIdentifier");
		IndexedQualifiedIdentifier theRet(thisElement->indexedLocalScopeIdentifier());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("IndexedQualifiedIdentifier")); }
};

struct KDevelop_DUContext_findContextAt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findContextAt");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		bool arg2=args[2].toReal().isTrue();
		KDevelop::DUContext* theRet(thisElement->findContextAt(arg1, arg2));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Bool).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_DUContext_findDeclarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findDeclarations");
		KDevelop::Identifier arg1=args[1].customObjectValue().value<KDevelop::Identifier >();
		KDevelop::CursorInRevision arg2=args[2].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::TopDUContext * arg3=args[3].customObjectValue().value<KDevelop::TopDUContext* >();
		SearchFlags arg4=args[4].customObjectValue().value<SearchFlags >();
		QList<Declaration*> theRet(thisElement->findDeclarations(arg1, arg2, arg3, arg4));
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::Identifier")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("SearchFlags")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Declaration*"))); }
};

struct KDevelop_DUContext_findDeclarationAt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findDeclarationAt");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::Declaration* theRet(thisElement->findDeclarationAt(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

struct KDevelop_DUContext_findLocalDeclarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findLocalDeclarations");
		KDevelop::Identifier arg1=args[1].customObjectValue().value<KDevelop::Identifier >();
		KDevelop::CursorInRevision arg2=args[2].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::TopDUContext * arg3=args[3].customObjectValue().value<KDevelop::TopDUContext* >();
		TypePtr<AbstractType> arg4=args[4].customObjectValue().value<TypePtr<AbstractType> >();
		SearchFlags arg5=args[5].customObjectValue().value<SearchFlags >();
		QList<Declaration*> theRet(thisElement->findLocalDeclarations(arg1, arg2, arg3, arg4, arg5));
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::Identifier")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType("SearchFlags")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Declaration*"))); }
};

struct KDevelop_DUContext_localScopeIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::localScopeIdentifier");
		KDevelop::QualifiedIdentifier theRet(thisElement->localScopeIdentifier());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_DUContext_childContexts : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::childContexts");
		QVector<DUContext*> theRet(thisElement->childContexts());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext*"))); }
};

struct KDevelop_DUContext_fullyApplyAliases : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::fullyApplyAliases");
		KDevelop::QualifiedIdentifier arg1=args[1].customObjectValue().value<KDevelop::QualifiedIdentifier >();
		KDevelop::TopDUContext * arg2=args[2].customObjectValue().value<KDevelop::TopDUContext* >();
		QList<QualifiedIdentifier> theRet(thisElement->fullyApplyAliases(arg1, arg2));
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("QualifiedIdentifier"))); }
};

struct KDevelop_DUContext_parentContextOf : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::parentContextOf");
		KDevelop::DUContext* arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		bool theRet(thisElement->parentContextOf(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_topContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::topContext");
		KDevelop::TopDUContext* theRet(thisElement->topContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct KDevelop_DUContext_findContextIncluding : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::findContextIncluding");
		KDevelop::RangeInRevision arg1=args[1].customObjectValue().value<KDevelop::RangeInRevision >();
		KDevelop::DUContext* theRet(thisElement->findContextIncluding(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_DUContext_usesCount : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::usesCount");
		int theRet(thisElement->usesCount());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_DUContext_equalScopeIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::equalScopeIdentifier");
		KDevelop::DUContext * arg1=args[1].customObjectValue().value<KDevelop::DUContext* >();
		bool theRet(thisElement->equalScopeIdentifier(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DUContext_type : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::type");
		KDevelop::DUContext::ContextType theRet(thisElement->type());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_DUContext_importedParentContexts : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::importedParentContexts");
		QVector<KDevelop::DUContext::Import> theRet(thisElement->importedParentContexts());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext::Import"))); }
};

struct KDevelop_DUContext_scopeIdentifier : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::scopeIdentifier");
		bool arg1=args[1].toReal().isTrue();
		KDevelop::QualifiedIdentifier theRet(thisElement->scopeIdentifier(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_DUContext_importers : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::importers");
		QVector<DUContext*> theRet(thisElement->importers());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext*"))); }
};

struct KDevelop_DUContext_parentContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* thisElement=args[0].customObjectValue().value<KDevelop::DUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUContext::parentContext");
		KDevelop::DUContext* theRet(thisElement->parentContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

//Generating KDevelop::IndexedDUContext

struct KDevelop_IndexedDUContext_isValid : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::isValid");
		bool theRet(thisElement->isValid());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_IndexedDUContext_localIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::localIndex");
		uint theRet(thisElement->localIndex());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_IndexedDUContext_dummyData : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::dummyData");
		QPair<uint,uint> theRet(thisElement->dummyData());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType("QPair<uint,uint>")); }
};

struct KDevelop_IndexedDUContext_indexedTopContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::indexedTopContext");
		KDevelop::IndexedTopDUContext theRet(thisElement->indexedTopContext());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType("KDevelop::IndexedTopDUContext")); }
};

struct KDevelop_IndexedDUContext_context : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::context");
		KDevelop::DUContext* theRet(thisElement->context());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_IndexedDUContext_data : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::data");
		KDevelop::DUContext* theRet(thisElement->data());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_IndexedDUContext_isDummy : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::isDummy");
		bool theRet(thisElement->isDummy());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_IndexedDUContext_topContextIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::topContextIndex");
		uint theRet(thisElement->topContextIndex());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_IndexedDUContext_hash : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedDUContext* thisElement=args[0].customObjectValue().value<KDevelop::IndexedDUContext* >();
		Q_ASSERT(thisElement && "calling KDevelop::IndexedDUContext::hash");
		uint theRet(thisElement->hash());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext*")).addParameter(ExpressionType::Value); }
};

//Generating KDevelop::AbstractType

struct KDevelop_AbstractType_toString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::toString");
		QString theRet(thisElement->toString());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_AbstractType_clone : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::clone");
		TypePtr<AbstractType> theRet(thisElement->clone());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType("TypePtr<AbstractType>")); }
};

struct KDevelop_AbstractType_indexed : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::indexed");
		KDevelop::IndexedType theRet(thisElement->indexed());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType("KDevelop::IndexedType")); }
};

struct KDevelop_AbstractType_whichType : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::whichType");
		KDevelop::AbstractType::WhichType theRet(thisElement->whichType());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType::Value); }
};

struct KDevelop_AbstractType_equals : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::equals");
		KDevelop::AbstractType * arg1=args[1].customObjectValue().value<KDevelop::AbstractType* >();
		bool theRet(thisElement->equals(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType("KDevelop::AbstractType*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_AbstractType_modifiers : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::modifiers");
		unsigned long long theRet(thisElement->modifiers());
		return Expression(Cn((uint) theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType::Value); }
};

struct KDevelop_AbstractType_hash : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		TypePtr<AbstractType> thisElement=args[0].customObjectValue().value<TypePtr<AbstractType> >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractType::hash");
		uint theRet(thisElement->hash());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("TypePtr<AbstractType>")).addParameter(ExpressionType::Value); }
};

//Generating KDevelop::ClassFunctionDeclaration

struct KDevelop_ClassFunctionDeclaration_defaultParametersSize : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::defaultParametersSize");
		unsigned int theRet(thisElement->defaultParametersSize());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_ClassFunctionDeclaration_toString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::toString");
		QString theRet(thisElement->toString());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_ClassFunctionDeclaration_isConstructor : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isConstructor");
		bool theRet(thisElement->isConstructor());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isSignal : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isSignal");
		bool theRet(thisElement->isSignal());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isAbstract : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isAbstract");
		bool theRet(thisElement->isAbstract());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isFinal : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isFinal");
		bool theRet(thisElement->isFinal());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isSlot : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isSlot");
		bool theRet(thisElement->isSlot());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_additionalIdentity : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::additionalIdentity");
		uint theRet(thisElement->additionalIdentity());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_ClassFunctionDeclaration_isFunctionDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isFunctionDeclaration");
		bool theRet(thisElement->isFunctionDeclaration());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isConversionFunction : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isConversionFunction");
		bool theRet(thisElement->isConversionFunction());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_isDestructor : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::isDestructor");
		bool theRet(thisElement->isDestructor());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassFunctionDeclaration_defaultParameters : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::defaultParameters");
		KDevelop::IndexedString const* theRet(thisElement->defaultParameters());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::IndexedString*")); }
};

struct KDevelop_ClassFunctionDeclaration_clonePrivate : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassFunctionDeclaration::clonePrivate");
		KDevelop::Declaration* theRet(thisElement->clonePrivate());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

//Generating KDevelop::ClassMemberDeclaration

struct KDevelop_ClassMemberDeclaration_isSynchronized : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isSynchronized");
		bool theRet(thisElement->isSynchronized());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isAbstract : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isAbstract");
		bool theRet(thisElement->isAbstract());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isFriend : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isFriend");
		bool theRet(thisElement->isFriend());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isStatic : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isStatic");
		bool theRet(thisElement->isStatic());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isNative : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isNative");
		bool theRet(thisElement->isNative());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isStrictFP : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isStrictFP");
		bool theRet(thisElement->isStrictFP());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isMutable : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isMutable");
		bool theRet(thisElement->isMutable());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_accessPolicy : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::accessPolicy");
		AccessPolicy theRet(thisElement->accessPolicy());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType("AccessPolicy")); }
};

struct KDevelop_ClassMemberDeclaration_isRegister : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isRegister");
		bool theRet(thisElement->isRegister());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isExtern : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isExtern");
		bool theRet(thisElement->isExtern());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_ClassMemberDeclaration_isAuto : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::ClassMemberDeclaration::isAuto");
		bool theRet(thisElement->isAuto());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

//Generating KDevelop::AbstractFunctionDeclaration

struct KDevelop_AbstractFunctionDeclaration_defaultParameterForArgument : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::defaultParameterForArgument");
		int arg1=args[1].toReal().intValue();
		KDevelop::IndexedString theRet(thisElement->defaultParameterForArgument(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::IndexedString")); }
};

struct KDevelop_AbstractFunctionDeclaration_defaultParametersSize : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::defaultParametersSize");
		unsigned int theRet(thisElement->defaultParametersSize());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_AbstractFunctionDeclaration_defaultParameters : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::defaultParameters");
		KDevelop::IndexedString const* theRet(thisElement->defaultParameters());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::IndexedString*")); }
};

struct KDevelop_AbstractFunctionDeclaration_isExplicit : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::isExplicit");
		bool theRet(thisElement->isExplicit());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_AbstractFunctionDeclaration_internalFunctionContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::internalFunctionContext");
		KDevelop::DUContext* theRet(thisElement->internalFunctionContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::DUContext*")); }
};

struct KDevelop_AbstractFunctionDeclaration_isInline : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::isInline");
		bool theRet(thisElement->isInline());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_AbstractFunctionDeclaration_isVirtual : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::AbstractFunctionDeclaration* thisElement=args[0].customObjectValue().value<KDevelop::AbstractFunctionDeclaration* >();
		Q_ASSERT(thisElement && "calling KDevelop::AbstractFunctionDeclaration::isVirtual");
		bool theRet(thisElement->isVirtual());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")).addParameter(ExpressionType::Bool); }
};

//Generating KDevelop::FunctionDefinition

struct KDevelop_FunctionDefinition_hasDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::FunctionDefinition* thisElement=args[0].customObjectValue().value<KDevelop::FunctionDefinition* >();
		Q_ASSERT(thisElement && "calling KDevelop::FunctionDefinition::hasDeclaration");
		bool theRet(thisElement->hasDeclaration());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::FunctionDefinition*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_FunctionDefinition_declaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::FunctionDefinition* thisElement=args[0].customObjectValue().value<KDevelop::FunctionDefinition* >();
		Q_ASSERT(thisElement && "calling KDevelop::FunctionDefinition::declaration");
		KDevelop::TopDUContext * arg1=args[1].customObjectValue().value<KDevelop::TopDUContext* >();
		KDevelop::Declaration* theRet(thisElement->declaration(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::FunctionDefinition*")).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::Declaration*")); }
};

//Generating KDevelop::DUChainBase

struct KDevelop_DUChainBase_topContext : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::topContext");
		KDevelop::TopDUContext* theRet(thisElement->topContext());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct KDevelop_DUChainBase_d_func_dynamic : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::d_func_dynamic");
		KDevelop::DUChainBaseData* theRet(thisElement->d_func_dynamic());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::DUChainBaseData*")); }
};

struct KDevelop_DUChainBase_rangeInCurrentRevision : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::rangeInCurrentRevision");
		KDevelop::SimpleRange theRet(thisElement->rangeInCurrentRevision());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::SimpleRange")); }
};

struct KDevelop_DUChainBase_range : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::range");
		KDevelop::RangeInRevision theRet(thisElement->range());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct KDevelop_DUChainBase_weakPointer : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::weakPointer");
		KSharedPtr<DUChainPointerData> const& theRet(thisElement->weakPointer());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KSharedPtr<DUChainPointerData>")); }
};

struct KDevelop_DUChainBase_transformToLocalRevision : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::transformToLocalRevision");
		KDevelop::SimpleRange arg1=args[1].customObjectValue().value<KDevelop::SimpleRange >();
		KDevelop::RangeInRevision theRet(thisElement->transformToLocalRevision(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::SimpleRange")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct KDevelop_DUChainBase_createRangeMoving : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::createRangeMoving");
		KSharedPtr<PersistentMovingRange> theRet(thisElement->createRangeMoving());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KSharedPtr<PersistentMovingRange>")); }
};

struct KDevelop_DUChainBase_url : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::url");
		KDevelop::IndexedString theRet(thisElement->url());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::IndexedString")); }
};

struct KDevelop_DUChainBase_transformFromLocalRevision : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUChainBase* thisElement=args[0].customObjectValue().value<KDevelop::DUChainBase* >();
		Q_ASSERT(thisElement && "calling KDevelop::DUChainBase::transformFromLocalRevision");
		KDevelop::RangeInRevision arg1=args[1].customObjectValue().value<KDevelop::RangeInRevision >();
		KDevelop::SimpleRange theRet(thisElement->transformFromLocalRevision(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType("KDevelop::SimpleRange")); }
};

//Generating KDevelop::QualifiedIdentifier

struct KDevelop_QualifiedIdentifier_beginsWith : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::beginsWith");
		KDevelop::QualifiedIdentifier arg1=args[1].customObjectValue().value<KDevelop::QualifiedIdentifier >();
		bool theRet(thisElement->beginsWith(arg1));
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_mid : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::mid");
		int arg1=args[1].toReal().intValue();
		int arg2=args[2].toReal().intValue();
		KDevelop::QualifiedIdentifier theRet(thisElement->mid(arg1, arg2));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_QualifiedIdentifier_first : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::first");
		KDevelop::Identifier theRet(thisElement->first());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("KDevelop::Identifier")); }
};

struct KDevelop_QualifiedIdentifier_isEmpty : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::isEmpty");
		bool theRet(thisElement->isEmpty());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_isExpression : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::isExpression");
		bool theRet(thisElement->isExpression());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_toStringList : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::toStringList");
		QStringList theRet(thisElement->toStringList());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("QStringList")); }
};

struct KDevelop_QualifiedIdentifier_inRepository : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::inRepository");
		bool theRet(thisElement->inRepository());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_last : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::last");
		KDevelop::Identifier theRet(thisElement->last());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("KDevelop::Identifier")); }
};

struct KDevelop_QualifiedIdentifier_top : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::top");
		KDevelop::Identifier theRet(thisElement->top());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("KDevelop::Identifier")); }
};

struct KDevelop_QualifiedIdentifier_index : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::index");
		uint theRet(thisElement->index());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_QualifiedIdentifier_toString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::toString");
		bool arg1=args[1].toReal().isTrue();
		QString theRet(thisElement->toString(arg1));
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_QualifiedIdentifier_left : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::left");
		int arg1=args[1].toReal().intValue();
		KDevelop::QualifiedIdentifier theRet(thisElement->left(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_QualifiedIdentifier_merge : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::merge");
		KDevelop::QualifiedIdentifier arg1=args[1].customObjectValue().value<KDevelop::QualifiedIdentifier >();
		KDevelop::QualifiedIdentifier theRet(thisElement->merge(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")); }
};

struct KDevelop_QualifiedIdentifier_hash : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::hash");
		uint theRet(thisElement->hash());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_QualifiedIdentifier_explicitlyGlobal : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::explicitlyGlobal");
		bool theRet(thisElement->explicitlyGlobal());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_count : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::count");
		int theRet(thisElement->count());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value); }
};

struct KDevelop_QualifiedIdentifier_isQualified : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::isQualified");
		bool theRet(thisElement->isQualified());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_QualifiedIdentifier_at : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::QualifiedIdentifier* thisElement=args[0].customObjectValue().value<KDevelop::QualifiedIdentifier* >();
		Q_ASSERT(thisElement && "calling KDevelop::QualifiedIdentifier::at");
		int arg1=args[1].toReal().intValue();
		KDevelop::Identifier const theRet(thisElement->at(arg1));
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::Identifierconst")); }
};

//Generating KDevelop::Problem

struct KDevelop_Problem_source : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::source");
		KDevelop::ProblemData::Source theRet(thisElement->source());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("KDevelop::ProblemData::Source")); }
};

struct KDevelop_Problem_severity : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::severity");
		KDevelop::ProblemData::Severity theRet(thisElement->severity());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("KDevelop::ProblemData::Severity")); }
};

struct KDevelop_Problem_toString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::toString");
		QString theRet(thisElement->toString());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_Problem_description : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::description");
		QString theRet(thisElement->description());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_Problem_solutionAssistant : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::solutionAssistant");
		KSharedPtr<IAssistant> theRet(thisElement->solutionAssistant());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("KSharedPtr<IAssistant>")); }
};

struct KDevelop_Problem_finalLocation : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::finalLocation");
		DocumentRange theRet(thisElement->finalLocation());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("DocumentRange")); }
};

struct KDevelop_Problem_sourceString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::sourceString");
		QString theRet(thisElement->sourceString());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct KDevelop_Problem_url : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::url");
		KDevelop::IndexedString theRet(thisElement->url());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("KDevelop::IndexedString")); }
};

struct KDevelop_Problem_locationStack : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::locationStack");
		QStack<DocumentCursor> theRet(thisElement->locationStack());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("QStack<DocumentCursor>")); }
};

struct KDevelop_Problem_explanation : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Problem* thisElement=args[0].customObjectValue().value<KDevelop::Problem* >();
		Q_ASSERT(thisElement && "calling KDevelop::Problem::explanation");
		QString theRet(thisElement->explanation());
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

//Generating KDevelop::CheckData

struct KDevelop_CheckData_access : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CheckData* thisElement=args[0].customObjectValue().value<KDevelop::CheckData* >();
		Q_ASSERT(thisElement && "calling KDevelop::CheckData::access");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->access),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CheckData*")).addParameter(ExpressionType("KDevelop::DataAccessRepository*")); }
};

struct KDevelop_CheckData_url : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CheckData* thisElement=args[0].customObjectValue().value<KDevelop::CheckData* >();
		Q_ASSERT(thisElement && "calling KDevelop::CheckData::url");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->url),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CheckData*")).addParameter(ExpressionType("KUrl")); }
};

struct KDevelop_CheckData_flow : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CheckData* thisElement=args[0].customObjectValue().value<KDevelop::CheckData* >();
		Q_ASSERT(thisElement && "calling KDevelop::CheckData::flow");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->flow),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CheckData*")).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")); }
};

struct KDevelop_CheckData_top : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CheckData* thisElement=args[0].customObjectValue().value<KDevelop::CheckData* >();
		Q_ASSERT(thisElement && "calling KDevelop::CheckData::top");
		return Expression::constructCustomObject(qVariantFromValue(thisElement->top),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CheckData*")).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

//Generating KDevelop::ControlFlowNode

struct KDevelop_ControlFlowNode_conditionRange : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowNode::conditionRange");
		KDevelop::RangeInRevision theRet(thisElement->conditionRange());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct KDevelop_ControlFlowNode_alternative : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowNode::alternative");
		KDevelop::ControlFlowNode* theRet(thisElement->alternative());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")); }
};

struct KDevelop_ControlFlowNode_next : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowNode::next");
		KDevelop::ControlFlowNode* theRet(thisElement->next());
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")); }
};

struct KDevelop_ControlFlowNode_nodeRange : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowNode::nodeRange");
		KDevelop::RangeInRevision theRet(thisElement->nodeRange());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct KDevelop_ControlFlowNode_type : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowNode::type");
		KDevelop::ControlFlowNode::Type theRet(thisElement->type());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType::Value); }
};

//Generating KDevelop::ControlFlowGraph

struct KDevelop_ControlFlowGraph_rootNodes : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowGraph* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowGraph* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowGraph::rootNodes");
		QList<ControlFlowNode*> theRet(thisElement->rootNodes());
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::ControlFlowNode*"))); }
};

struct KDevelop_ControlFlowGraph_deadNodes : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowGraph* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowGraph* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowGraph::deadNodes");
		QVector<ControlFlowNode*> theRet(thisElement->deadNodes());
		return Expression::constructList(toList(theRet.toList()));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::ControlFlowNode*"))); }
};

struct KDevelop_ControlFlowGraph_nodeForDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowGraph* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowGraph* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowGraph::nodeForDeclaration");
		KDevelop::Declaration* arg1=args[1].customObjectValue().value<KDevelop::Declaration* >();
		KDevelop::ControlFlowNode* theRet(thisElement->nodeForDeclaration(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")); }
};

struct KDevelop_ControlFlowGraph_declarations : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowGraph* thisElement=args[0].customObjectValue().value<KDevelop::ControlFlowGraph* >();
		Q_ASSERT(thisElement && "calling KDevelop::ControlFlowGraph::declarations");
		QList<KDevelop::Declaration*> theRet(thisElement->declarations());
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Declaration*"))); }
};

//Generating KDevelop::DataAccess

struct KDevelop_DataAccess_flags : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::flags");
		KDevelop::DataAccess::DataAccessFlags theRet(thisElement->flags());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType("KDevelop::DataAccess::DataAccessFlags")); }
};

struct KDevelop_DataAccess_isCall : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::isCall");
		bool theRet(thisElement->isCall());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DataAccess_value : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::value");
		KDevelop::RangeInRevision theRet(thisElement->value());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct KDevelop_DataAccess_pos : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::pos");
		KDevelop::CursorInRevision theRet(thisElement->pos());
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct KDevelop_DataAccess_isRead : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::isRead");
		bool theRet(thisElement->isRead());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType::Bool); }
};

struct KDevelop_DataAccess_isWrite : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccess* thisElement=args[0].customObjectValue().value<KDevelop::DataAccess* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccess::isWrite");
		bool theRet(thisElement->isWrite());
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccess*")).addParameter(ExpressionType::Bool); }
};

//Generating KDevelop::DataAccessRepository

struct KDevelop_DataAccessRepository_modifications : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccessRepository* thisElement=args[0].customObjectValue().value<KDevelop::DataAccessRepository* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccessRepository::modifications");
		QList<DataAccess*> theRet(thisElement->modifications());
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccessRepository*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DataAccess*"))); }
};

struct KDevelop_DataAccessRepository_accessesInRange : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccessRepository* thisElement=args[0].customObjectValue().value<KDevelop::DataAccessRepository* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccessRepository::accessesInRange");
		KDevelop::RangeInRevision arg1=args[1].customObjectValue().value<KDevelop::RangeInRevision >();
		QList<DataAccess*> theRet(thisElement->accessesInRange(arg1));
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccessRepository*")).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DataAccess*"))); }
};

struct KDevelop_DataAccessRepository_accessAt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DataAccessRepository* thisElement=args[0].customObjectValue().value<KDevelop::DataAccessRepository* >();
		Q_ASSERT(thisElement && "calling KDevelop::DataAccessRepository::accessAt");
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::DataAccess* theRet(thisElement->accessAt(arg1));
		return theRet ? Expression::constructCustomObject(qVariantFromValue(theRet),0) : Expression::constructCustomObject(QVariant(), 0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DataAccessRepository*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::DataAccess*")); }
};

struct Helpers_usesForCtx : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext * arg0=args[0].customObjectValue().value<KDevelop::DUContext* >();
		QList<KDevelop::Use*> theRet = Helpers::usesForCtx(arg0);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Use*"))); }
};

struct Helpers_canGo : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* arg0=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		KDevelop::ControlFlowNode* arg1=args[1].customObjectValue().value<KDevelop::ControlFlowNode* >();
		bool theRet = Helpers::canGo(arg0, arg1);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType::Bool); }
};

struct Helpers_rangeIsValid : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		bool theRet = Helpers::rangeIsValid(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType::Bool); }
};

struct Helpers_Problem : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		QString arg1=args[1].stringValue();
		KSharedPtr<KDevelop::Problem> theRet = Helpers::Problem(arg0, arg1);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))).addParameter(ExpressionType("KSharedPtr<KDevelop::Problem>")); }
};

struct Helpers_allConditionNodes : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		QList<ControlFlowNode*> arg0=fromList<ControlFlowNode* >(args[0].toExpressionList());
		QList<KDevelop::ControlFlowNode*> theRet = Helpers::allConditionNodes(arg0);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::ControlFlowNode*"))).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::ControlFlowNode*"))); }
};

struct Helpers_canGoExt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowNode* arg0=args[0].customObjectValue().value<KDevelop::ControlFlowNode* >();
		KDevelop::ControlFlowNode* arg1=args[1].customObjectValue().value<KDevelop::ControlFlowNode* >();
		QList<KDevelop::CursorInRevision> arg2=fromList<KDevelop::CursorInRevision >(args[2].toExpressionList());
		bool theRet = Helpers::canGoExt(arg0, arg1, arg2);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::CursorInRevision"))).addParameter(ExpressionType::Bool); }
};

struct Helpers_rangeIsEmpty : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		bool theRet = Helpers::rangeIsEmpty(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType::Bool); }
};

struct Helpers_invalidCursor : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		Q_UNUSED(args)
		KDevelop::CursorInRevision theRet = Helpers::invalidCursor();
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct Helpers_rangeStart : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		KDevelop::CursorInRevision theRet = Helpers::rangeStart(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct Helpers_rangeToString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		QString theRet = Helpers::rangeToString(arg0);
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct Helpers_indexedStringToKUrl : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedString arg0=args[0].customObjectValue().value<KDevelop::IndexedString >();
		KUrl theRet = Helpers::indexedStringToKUrl(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedString")).addParameter(ExpressionType("KUrl")); }
};

struct Helpers_cursorToString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CursorInRevision arg0=args[0].customObjectValue().value<KDevelop::CursorInRevision >();
		QString theRet = Helpers::cursorToString(arg0);
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct Helpers_cursorIsValid : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CursorInRevision arg0=args[0].customObjectValue().value<KDevelop::CursorInRevision >();
		bool theRet = Helpers::cursorIsValid(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Bool); }
};

struct Helpers_rangeContains : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		bool theRet = Helpers::rangeContains(arg0, arg1);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Bool); }
};

struct Helpers_cursorLine : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CursorInRevision arg0=args[0].customObjectValue().value<KDevelop::CursorInRevision >();
		int theRet = Helpers::cursorLine(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Value); }
};

struct Helpers_useRangesForDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration * arg0=args[0].customObjectValue().value<KDevelop::Declaration* >();
		KUrl arg1=args[1].customObjectValue().value<KUrl >();
		QList<KDevelop::RangeInRevision> theRet = Helpers::useRangesForDeclaration(arg0, arg1);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KUrl")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::RangeInRevision"))); }
};

struct Helpers_nodeAt : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ControlFlowGraph* arg0=args[0].customObjectValue().value<KDevelop::ControlFlowGraph* >();
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::ControlFlowNode* theRet = Helpers::nodeAt(arg0, arg1);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ControlFlowGraph*")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::ControlFlowNode*")); }
};

struct Helpers_bitAnd : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		int arg0=args[0].toReal().intValue();
		int arg1=args[1].toReal().intValue();
		bool theRet = Helpers::bitAnd(arg0, arg1);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType::Value).addParameter(ExpressionType::Value).addParameter(ExpressionType::Bool); }
};

struct Helpers_rangeEnd : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::RangeInRevision arg0=args[0].customObjectValue().value<KDevelop::RangeInRevision >();
		KDevelop::CursorInRevision theRet = Helpers::rangeEnd(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType("KDevelop::CursorInRevision")); }
};

struct Helpers_usesIn : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext * arg0=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		QList<KDevelop::Use*> theRet = Helpers::usesIn(arg0);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Use*"))); }
};

struct Helpers_usesInRange : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext * arg0=args[0].customObjectValue().value<KDevelop::DUContext* >();
		KDevelop::RangeInRevision arg1=args[1].customObjectValue().value<KDevelop::RangeInRevision >();
		QList<KDevelop::Use*> theRet = Helpers::usesInRange(arg0, arg1);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::RangeInRevision")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::Use*"))); }
};

struct Helpers_cursorColumn : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CursorInRevision arg0=args[0].customObjectValue().value<KDevelop::CursorInRevision >();
		int theRet = Helpers::cursorColumn(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType::Value); }
};

struct Helpers_KDevelop_DeclarationToKDevelop_FunctionDefinition : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* arg0=args[0].customObjectValue().value<KDevelop::Declaration* >();
		KDevelop::FunctionDefinition* theRet = Helpers::KDevelop_DeclarationToKDevelop_FunctionDefinition(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::FunctionDefinition*")); }
};

struct Helpers_RangeInRevision : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::CursorInRevision arg0=args[0].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::CursorInRevision arg1=args[1].customObjectValue().value<KDevelop::CursorInRevision >();
		KDevelop::RangeInRevision theRet = Helpers::RangeInRevision(arg0, arg1);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::CursorInRevision")).addParameter(ExpressionType("KDevelop::RangeInRevision")); }
};

struct Helpers_KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassFunctionDeclaration* arg0=args[0].customObjectValue().value<KDevelop::ClassFunctionDeclaration* >();
		KDevelop::AbstractFunctionDeclaration* theRet = Helpers::KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::AbstractFunctionDeclaration*")); }
};

struct Helpers_imported : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::TopDUContext* arg0=args[0].customObjectValue().value<KDevelop::TopDUContext* >();
		QList<KDevelop::DUContext::Import> theRet = Helpers::imported(arg0);
		return Expression::constructList(toList(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType(ExpressionType::List, ExpressionType("KDevelop::DUContext::Import"))); }
};

struct Helpers_indexedStringToString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::IndexedString arg0=args[0].customObjectValue().value<KDevelop::IndexedString >();
		QString theRet = Helpers::indexedStringToString(arg0);
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedString")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct Helpers_KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* arg0=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		bool theRet = Helpers::KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType::Bool); }
};

struct Helpers_identifierToString : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Identifier arg0=args[0].customObjectValue().value<KDevelop::Identifier >();
		QString theRet = Helpers::identifierToString(arg0);
		return Expression::constructString(theRet);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Identifier")).addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char))); }
};

struct Helpers_KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::ClassMemberDeclaration* arg0=args[0].customObjectValue().value<KDevelop::ClassMemberDeclaration* >();
		KDevelop::ClassFunctionDeclaration* theRet = Helpers::KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")); }
};

struct Helpers_useForIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::DUContext* arg0=args[0].customObjectValue().value<KDevelop::DUContext* >();
		int arg1=args[1].toReal().intValue();
		KDevelop::Use* theRet = Helpers::useForIndex(arg0, arg1);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::Use*")); }
};

struct Helpers_fromIndex : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		uint arg0=args[0].toReal().intValue();
		KDevelop::TopDUContext* theRet = Helpers::fromIndex(arg0);
		return Expression::constructCustomObject(qVariantFromValue(theRet),0);
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType::Value).addParameter(ExpressionType("KDevelop::TopDUContext*")); }
};

struct Helpers_KDevelop_DeclarationIsKDevelop_FunctionDefinition : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		KDevelop::Declaration* arg0=args[0].customObjectValue().value<KDevelop::Declaration* >();
		bool theRet = Helpers::KDevelop_DeclarationIsKDevelop_FunctionDefinition(arg0);
		return Expression(Cn(theRet));
	}

	static ExpressionType type() { return ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool); }
};

#include "bindings.h"
#include <analitza/variables.h>

template <class T>
QList<Expression> toList(const QList< T >& returned)
{
	QList<Expression> ret;
	foreach(const T& element, returned) {
		ret.append(Expression::constructCustomObject(QVariant::fromValue<T>(element), 0));
	}
	return ret;
}

template <class T>
QList<T> fromList(const QList<Expression>& exps)
{
	QList<T> ret;
	foreach(const Expression& element, exps) {
		ret.append(element.customObjectValue().value<T>());
	}
	return ret;
}

template <class T>
struct RefQVariantType : public Analitza::FunctionDefinition
{
	static void deleter(const QVariant& v) { delete v.value<T*>(); }
	
	virtual Expression operator()(const QList<Expression>& args)
	{
		T v = args.first().customObjectValue().value<T>();
		T* pv = new T(v);
		
		return Expression::constructCustomObject(QVariant::fromValue<T*>(pv), deleter);
	}
};

struct Debug : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		qDebug() << args.first().stringValue() << args.last().toString();
		return args.last();
	}

	static ExpressionType type() {
		return ExpressionType(ExpressionType::Lambda)
			.addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char)))
			.addParameter(ExpressionType(ExpressionType::Any,1))
			.addParameter(ExpressionType(ExpressionType::Any,1));
	}
};

struct Debug2 : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		qDebug() << args.first().stringValue() << args[1].toString();
		return args.last();
	}

	static ExpressionType type() {
		return ExpressionType(ExpressionType::Lambda)
			.addParameter(ExpressionType(ExpressionType::List, ExpressionType(ExpressionType::Char)))
			.addParameter(ExpressionType(ExpressionType::Any,2))
			.addParameter(ExpressionType(ExpressionType::Any,1))
			.addParameter(ExpressionType(ExpressionType::Any,1));
	}
};

struct ValidCustomValue : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		bool valid=!args.first().isCustomObject() || args.first().customObjectValue().isValid();
		
		return Expression(Cn(valid));
	}

	static ExpressionType type() {
		return ExpressionType(ExpressionType::Lambda)
			.addParameter(ExpressionType(ExpressionType::Any,1))
			.addParameter(ExpressionType(ExpressionType::Bool));
	}
};

template <class T, class U>
struct Cast : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		//TODO: maintain customobject refcounting
		U* v = args.first().customObjectValue().value<T*>();
		
		return Expression::constructCustomObject(QVariant::fromValue<U*>(v), 0/*, construct refcount*/);
	}
};

template <class T, class U>
struct DownCast : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		//TODO: maintain customobject refcounting
		T* v = args.first().customObjectValue().value<T*>();
		
		Q_ASSERT(dynamic_cast<U*>(v));
		
		return Expression::constructCustomObject(QVariant::fromValue<U*>(static_cast<U*>(v)), 0/*, construct refcount*/);
	}
};

template <class T, class U>
struct CanCast : public Analitza::FunctionDefinition
{
	virtual Expression operator()(const QList<Expression>& args)
	{
		T* v = args.first().customObjectValue().value<T*>();
		
		return Expression(Cn(dynamic_cast<U*>(v)!=0));
	}
};

void registerFunctions(Analitza::Variables* variables, Analitza::BuiltinMethods* builtin)
{
	builtin->insertFunction("debug", Debug::type(), new Debug);
	builtin->insertFunction("debug2", Debug2::type(), new Debug2);
	builtin->insertFunction("valid", ValidCustomValue::type(), new ValidCustomValue);
	builtin->insertFunction("KDevelop_DeclarationToKDevelop_DUChainBase", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::DUChainBase*")), new Cast<KDevelop::Declaration, KDevelop::DUChainBase>);
	builtin->insertFunction("KDevelop_DUChainBaseToKDevelop_Declaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::Declaration*")), new DownCast<KDevelop::DUChainBase, KDevelop::Declaration>);
	builtin->insertFunction("KDevelop_DUChainBaseIsKDevelop_Declaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::DUChainBase, KDevelop::Declaration>);
	variables->modify("KDevelop_Declaration_Public", Cn(KDevelop::Declaration::Public));
	variables->modify("KDevelop_Declaration_Protected", Cn(KDevelop::Declaration::Protected));
	variables->modify("KDevelop_Declaration_Private", Cn(KDevelop::Declaration::Private));
	variables->modify("KDevelop_Declaration_DefaultAccess", Cn(KDevelop::Declaration::DefaultAccess));
	variables->modify("KDevelop_Declaration_Type", Cn(KDevelop::Declaration::Type));
	variables->modify("KDevelop_Declaration_Instance", Cn(KDevelop::Declaration::Instance));
	variables->modify("KDevelop_Declaration_NamespaceAlias", Cn(KDevelop::Declaration::NamespaceAlias));
	variables->modify("KDevelop_Declaration_Alias", Cn(KDevelop::Declaration::Alias));
	variables->modify("KDevelop_Declaration_Namespace", Cn(KDevelop::Declaration::Namespace));
	variables->modify("KDevelop_Declaration_Import", Cn(KDevelop::Declaration::Import));
	variables->modify("KDevelop_Declaration_Identity", Cn(KDevelop::Declaration::Identity));
	builtin->insertFunction("KDevelop_Declaration_isDefinition", KDevelop_Declaration_isDefinition::type(), new KDevelop_Declaration_isDefinition);
	builtin->insertFunction("KDevelop_Declaration_ownIndex", KDevelop_Declaration_ownIndex::type(), new KDevelop_Declaration_ownIndex);
	builtin->insertFunction("KDevelop_Declaration_equalQualifiedIdentifier", KDevelop_Declaration_equalQualifiedIdentifier::type(), new KDevelop_Declaration_equalQualifiedIdentifier);
	builtin->insertFunction("KDevelop_Declaration_logicalInternalContext", KDevelop_Declaration_logicalInternalContext::type(), new KDevelop_Declaration_logicalInternalContext);
	builtin->insertFunction("KDevelop_Declaration_hasUses", KDevelop_Declaration_hasUses::type(), new KDevelop_Declaration_hasUses);
	builtin->insertFunction("KDevelop_Declaration_isForwardDeclaration", KDevelop_Declaration_isForwardDeclaration::type(), new KDevelop_Declaration_isForwardDeclaration);
	builtin->insertFunction("KDevelop_Declaration_identifier", KDevelop_Declaration_identifier::type(), new KDevelop_Declaration_identifier);
	builtin->insertFunction("KDevelop_Declaration_clone", KDevelop_Declaration_clone::type(), new KDevelop_Declaration_clone);
	builtin->insertFunction("KDevelop_Declaration_uses", KDevelop_Declaration_uses::type(), new KDevelop_Declaration_uses);
	builtin->insertFunction("KDevelop_Declaration_indexedType", KDevelop_Declaration_indexedType::type(), new KDevelop_Declaration_indexedType);
	builtin->insertFunction("KDevelop_Declaration_additionalIdentity", KDevelop_Declaration_additionalIdentity::type(), new KDevelop_Declaration_additionalIdentity);
	builtin->insertFunction("KDevelop_Declaration_isExplicitlyDeleted", KDevelop_Declaration_isExplicitlyDeleted::type(), new KDevelop_Declaration_isExplicitlyDeleted);
	builtin->insertFunction("KDevelop_Declaration_inDUChain", KDevelop_Declaration_inDUChain::type(), new KDevelop_Declaration_inDUChain);
	builtin->insertFunction("KDevelop_Declaration_specialization", KDevelop_Declaration_specialization::type(), new KDevelop_Declaration_specialization);
	builtin->insertFunction("KDevelop_Declaration_isFinal", KDevelop_Declaration_isFinal::type(), new KDevelop_Declaration_isFinal);
	builtin->insertFunction("KDevelop_Declaration_isAnonymous", KDevelop_Declaration_isAnonymous::type(), new KDevelop_Declaration_isAnonymous);
	builtin->insertFunction("KDevelop_Declaration_comment", KDevelop_Declaration_comment::type(), new KDevelop_Declaration_comment);
	builtin->insertFunction("KDevelop_Declaration_internalContext", KDevelop_Declaration_internalContext::type(), new KDevelop_Declaration_internalContext);
	builtin->insertFunction("KDevelop_Declaration_inSymbolTable", KDevelop_Declaration_inSymbolTable::type(), new KDevelop_Declaration_inSymbolTable);
	builtin->insertFunction("KDevelop_Declaration_usesCurrentRevision", KDevelop_Declaration_usesCurrentRevision::type(), new KDevelop_Declaration_usesCurrentRevision);
	builtin->insertFunction("KDevelop_Declaration_toForwardDeclaration", KDevelop_Declaration_toForwardDeclaration::type(), new KDevelop_Declaration_toForwardDeclaration);
	builtin->insertFunction("KDevelop_Declaration_isAutoDeclaration", KDevelop_Declaration_isAutoDeclaration::type(), new KDevelop_Declaration_isAutoDeclaration);
	builtin->insertFunction("KDevelop_Declaration_abstractType", KDevelop_Declaration_abstractType::type(), new KDevelop_Declaration_abstractType);
	builtin->insertFunction("KDevelop_Declaration_id", KDevelop_Declaration_id::type(), new KDevelop_Declaration_id);
	builtin->insertFunction("KDevelop_Declaration_indexedIdentifier", KDevelop_Declaration_indexedIdentifier::type(), new KDevelop_Declaration_indexedIdentifier);
	builtin->insertFunction("KDevelop_Declaration_toString", KDevelop_Declaration_toString::type(), new KDevelop_Declaration_toString);
	builtin->insertFunction("KDevelop_Declaration_topContext", KDevelop_Declaration_topContext::type(), new KDevelop_Declaration_topContext);
	builtin->insertFunction("KDevelop_Declaration_alwaysForceDirect", KDevelop_Declaration_alwaysForceDirect::type(), new KDevelop_Declaration_alwaysForceDirect);
	builtin->insertFunction("KDevelop_Declaration_context", KDevelop_Declaration_context::type(), new KDevelop_Declaration_context);
	builtin->insertFunction("KDevelop_Declaration_logicalDeclaration", KDevelop_Declaration_logicalDeclaration::type(), new KDevelop_Declaration_logicalDeclaration);
	builtin->insertFunction("KDevelop_Declaration_isTypeAlias", KDevelop_Declaration_isTypeAlias::type(), new KDevelop_Declaration_isTypeAlias);
	builtin->insertFunction("KDevelop_Declaration_isFunctionDeclaration", KDevelop_Declaration_isFunctionDeclaration::type(), new KDevelop_Declaration_isFunctionDeclaration);
	builtin->insertFunction("KDevelop_Declaration_qualifiedIdentifier", KDevelop_Declaration_qualifiedIdentifier::type(), new KDevelop_Declaration_qualifiedIdentifier);
	builtin->insertFunction("KDevelop_Declaration_kind", KDevelop_Declaration_kind::type(), new KDevelop_Declaration_kind);
	builtin->insertFunction("KDevelop_Use_usedDeclaration", KDevelop_Use_usedDeclaration::type(), new KDevelop_Use_usedDeclaration);
	builtin->insertFunction("KDevelop_Use_m_declarationIndex", KDevelop_Use_m_declarationIndex::type(), new KDevelop_Use_m_declarationIndex);
	builtin->insertFunction("KDevelop_Use_m_range", KDevelop_Use_m_range::type(), new KDevelop_Use_m_range);
	builtin->insertFunction("KDevelop_TopDUContextToKDevelop_DUContext", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::TopDUContext*")).addParameter(ExpressionType("KDevelop::DUContext*")), new Cast<KDevelop::TopDUContext, KDevelop::DUContext>);
	builtin->insertFunction("KDevelop_DUContextToKDevelop_TopDUContext", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::TopDUContext*")), new DownCast<KDevelop::DUContext, KDevelop::TopDUContext>);
	builtin->insertFunction("KDevelop_DUContextIsKDevelop_TopDUContext", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::DUContext, KDevelop::TopDUContext>);
	variables->modify("KDevelop_TopDUContext_Empty", Cn(KDevelop::TopDUContext::Empty));
	variables->modify("KDevelop_TopDUContext_SimplifiedVisibleDeclarationsAndContexts", Cn(KDevelop::TopDUContext::SimplifiedVisibleDeclarationsAndContexts));
	variables->modify("KDevelop_TopDUContext_VisibleDeclarationsAndContexts", Cn(KDevelop::TopDUContext::VisibleDeclarationsAndContexts));
	variables->modify("KDevelop_TopDUContext_AllDeclarationsAndContexts", Cn(KDevelop::TopDUContext::AllDeclarationsAndContexts));
	variables->modify("KDevelop_TopDUContext_AllDeclarationsContextsAndUses", Cn(KDevelop::TopDUContext::AllDeclarationsContextsAndUses));
	variables->modify("KDevelop_TopDUContext_AST", Cn(KDevelop::TopDUContext::AST));
	variables->modify("KDevelop_TopDUContext_AllDeclarationsContextsUsesAndAST", Cn(KDevelop::TopDUContext::AllDeclarationsContextsUsesAndAST));
	variables->modify("KDevelop_TopDUContext_Recursive", Cn(KDevelop::TopDUContext::Recursive));
	variables->modify("KDevelop_TopDUContext_ForceUpdate", Cn(KDevelop::TopDUContext::ForceUpdate));
	variables->modify("KDevelop_TopDUContext_ForceUpdateRecursive", Cn(KDevelop::TopDUContext::ForceUpdateRecursive));
	variables->modify("KDevelop_TopDUContext_LastFeature", Cn(KDevelop::TopDUContext::LastFeature));
	variables->modify("KDevelop_TopDUContext_NoFlags", Cn(KDevelop::TopDUContext::NoFlags));
	variables->modify("KDevelop_TopDUContext_UpdatingContext", Cn(KDevelop::TopDUContext::UpdatingContext));
	variables->modify("KDevelop_TopDUContext_LastFlag", Cn(KDevelop::TopDUContext::LastFlag));
	variables->modify("KDevelop_TopDUContext_Identity", Cn(KDevelop::TopDUContext::Identity));
	builtin->insertFunction("KDevelop_TopDUContext_imports", KDevelop_TopDUContext_imports::type(), new KDevelop_TopDUContext_imports);
	builtin->insertFunction("KDevelop_TopDUContext_ownIndex", KDevelop_TopDUContext_ownIndex::type(), new KDevelop_TopDUContext_ownIndex);
	builtin->insertFunction("KDevelop_TopDUContext_sharedDataOwner", KDevelop_TopDUContext_sharedDataOwner::type(), new KDevelop_TopDUContext_sharedDataOwner);
	builtin->insertFunction("KDevelop_TopDUContext_parsingEnvironmentFile", KDevelop_TopDUContext_parsingEnvironmentFile::type(), new KDevelop_TopDUContext_parsingEnvironmentFile);
	builtin->insertFunction("KDevelop_TopDUContext_topContext", KDevelop_TopDUContext_topContext::type(), new KDevelop_TopDUContext_topContext);
	builtin->insertFunction("KDevelop_TopDUContext_inDUChain", KDevelop_TopDUContext_inDUChain::type(), new KDevelop_TopDUContext_inDUChain);
	builtin->insertFunction("KDevelop_TopDUContext_indexed", KDevelop_TopDUContext_indexed::type(), new KDevelop_TopDUContext_indexed);
	builtin->insertFunction("KDevelop_TopDUContext_importPosition", KDevelop_TopDUContext_importPosition::type(), new KDevelop_TopDUContext_importPosition);
	builtin->insertFunction("KDevelop_TopDUContext_isOnDisk", KDevelop_TopDUContext_isOnDisk::type(), new KDevelop_TopDUContext_isOnDisk);
	builtin->insertFunction("KDevelop_TopDUContext_deleting", KDevelop_TopDUContext_deleting::type(), new KDevelop_TopDUContext_deleting);
	builtin->insertFunction("KDevelop_TopDUContext_loadedImporters", KDevelop_TopDUContext_loadedImporters::type(), new KDevelop_TopDUContext_loadedImporters);
	builtin->insertFunction("KDevelop_TopDUContext_flags", KDevelop_TopDUContext_flags::type(), new KDevelop_TopDUContext_flags);
	builtin->insertFunction("KDevelop_TopDUContext_features", KDevelop_TopDUContext_features::type(), new KDevelop_TopDUContext_features);
	builtin->insertFunction("KDevelop_TopDUContext_problems", KDevelop_TopDUContext_problems::type(), new KDevelop_TopDUContext_problems);
	builtin->insertFunction("KDevelop_TopDUContext_importedParentContexts", KDevelop_TopDUContext_importedParentContexts::type(), new KDevelop_TopDUContext_importedParentContexts);
	builtin->insertFunction("KDevelop_TopDUContext_importers", KDevelop_TopDUContext_importers::type(), new KDevelop_TopDUContext_importers);
	builtin->insertFunction("KDevelop_TopDUContext_ast", KDevelop_TopDUContext_ast::type(), new KDevelop_TopDUContext_ast);
	builtin->insertFunction("KDevelop_TopDUContext_usingImportsCache", KDevelop_TopDUContext_usingImportsCache::type(), new KDevelop_TopDUContext_usingImportsCache);
	builtin->insertFunction("KDevelop_TopDUContext_recursiveImportIndices", KDevelop_TopDUContext_recursiveImportIndices::type(), new KDevelop_TopDUContext_recursiveImportIndices);
	builtin->insertFunction("KDevelop_TopDUContext_url", KDevelop_TopDUContext_url::type(), new KDevelop_TopDUContext_url);
	builtin->insertFunction("KDevelop_TopDUContext_usedDeclarationForIndex", KDevelop_TopDUContext_usedDeclarationForIndex::type(), new KDevelop_TopDUContext_usedDeclarationForIndex);
	builtin->insertFunction("KDevelop_DUContext_Import_indexedContext", KDevelop_DUContext_Import_indexedContext::type(), new KDevelop_DUContext_Import_indexedContext);
	builtin->insertFunction("KDevelop_DUContext_Import_isDirect", KDevelop_DUContext_Import_isDirect::type(), new KDevelop_DUContext_Import_isDirect);
	builtin->insertFunction("KDevelop_DUContext_Import_context", KDevelop_DUContext_Import_context::type(), new KDevelop_DUContext_Import_context);
	builtin->insertFunction("KDevelop_DUContext_Import_topContextIndex", KDevelop_DUContext_Import_topContextIndex::type(), new KDevelop_DUContext_Import_topContextIndex);
	builtin->insertFunction("KDevelop_DUContext_Import_indirectDeclarationId", KDevelop_DUContext_Import_indirectDeclarationId::type(), new KDevelop_DUContext_Import_indirectDeclarationId);
	builtin->insertFunction("KDevelop_DUContext_Import_position", KDevelop_DUContext_Import_position::type(), new KDevelop_DUContext_Import_position);
	builtin->insertFunction("KDevelop_DUContextToKDevelop_DUChainBase", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext*")).addParameter(ExpressionType("KDevelop::DUChainBase*")), new Cast<KDevelop::DUContext, KDevelop::DUChainBase>);
	builtin->insertFunction("KDevelop_DUChainBaseToKDevelop_DUContext", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::DUContext*")), new DownCast<KDevelop::DUChainBase, KDevelop::DUContext>);
	builtin->insertFunction("KDevelop_DUChainBaseIsKDevelop_DUContext", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::DUChainBase, KDevelop::DUContext>);
	variables->modify("KDevelop_DUContext_Identity", Cn(KDevelop::DUContext::Identity));
	variables->modify("KDevelop_DUContext_Global", Cn(KDevelop::DUContext::Global));
	variables->modify("KDevelop_DUContext_Namespace", Cn(KDevelop::DUContext::Namespace));
	variables->modify("KDevelop_DUContext_Class", Cn(KDevelop::DUContext::Class));
	variables->modify("KDevelop_DUContext_Function", Cn(KDevelop::DUContext::Function));
	variables->modify("KDevelop_DUContext_Template", Cn(KDevelop::DUContext::Template));
	variables->modify("KDevelop_DUContext_Enum", Cn(KDevelop::DUContext::Enum));
	variables->modify("KDevelop_DUContext_Helper", Cn(KDevelop::DUContext::Helper));
	variables->modify("KDevelop_DUContext_Other", Cn(KDevelop::DUContext::Other));
	variables->modify("KDevelop_DUContext_NoSearchFlags", Cn(KDevelop::DUContext::NoSearchFlags));
	variables->modify("KDevelop_DUContext_InImportedParentContext", Cn(KDevelop::DUContext::InImportedParentContext));
	variables->modify("KDevelop_DUContext_OnlyContainerTypes", Cn(KDevelop::DUContext::OnlyContainerTypes));
	variables->modify("KDevelop_DUContext_DontSearchInParent", Cn(KDevelop::DUContext::DontSearchInParent));
	variables->modify("KDevelop_DUContext_NoUndefinedTemplateParams", Cn(KDevelop::DUContext::NoUndefinedTemplateParams));
	variables->modify("KDevelop_DUContext_DirectQualifiedLookup", Cn(KDevelop::DUContext::DirectQualifiedLookup));
	variables->modify("KDevelop_DUContext_NoFiltering", Cn(KDevelop::DUContext::NoFiltering));
	variables->modify("KDevelop_DUContext_OnlyFunctions", Cn(KDevelop::DUContext::OnlyFunctions));
	variables->modify("KDevelop_DUContext_NoImportsCheck", Cn(KDevelop::DUContext::NoImportsCheck));
	variables->modify("KDevelop_DUContext_NoSelfLookUp", Cn(KDevelop::DUContext::NoSelfLookUp));
	variables->modify("KDevelop_DUContext_DontResolveAliases", Cn(KDevelop::DUContext::DontResolveAliases));
	variables->modify("KDevelop_DUContext_LastSearchFlag", Cn(KDevelop::DUContext::LastSearchFlag));
	builtin->insertFunction("KDevelop_DUContext_isPropagateDeclarations", KDevelop_DUContext_isPropagateDeclarations::type(), new KDevelop_DUContext_isPropagateDeclarations);
	builtin->insertFunction("KDevelop_DUContext_localDeclarations", KDevelop_DUContext_localDeclarations::type(), new KDevelop_DUContext_localDeclarations);
	builtin->insertFunction("KDevelop_DUContext_indexedImporters", KDevelop_DUContext_indexedImporters::type(), new KDevelop_DUContext_indexedImporters);
	builtin->insertFunction("KDevelop_DUContext_allDeclarations", KDevelop_DUContext_allDeclarations::type(), new KDevelop_DUContext_allDeclarations);
	builtin->insertFunction("KDevelop_DUContext_importPosition", KDevelop_DUContext_importPosition::type(), new KDevelop_DUContext_importPosition);
	builtin->insertFunction("KDevelop_DUContext_uses", KDevelop_DUContext_uses::type(), new KDevelop_DUContext_uses);
	builtin->insertFunction("KDevelop_DUContext_findUseAt", KDevelop_DUContext_findUseAt::type(), new KDevelop_DUContext_findUseAt);
	builtin->insertFunction("KDevelop_DUContext_depth", KDevelop_DUContext_depth::type(), new KDevelop_DUContext_depth);
	builtin->insertFunction("KDevelop_DUContext_findContext", KDevelop_DUContext_findContext::type(), new KDevelop_DUContext_findContext);
	builtin->insertFunction("KDevelop_DUContext_imports", KDevelop_DUContext_imports::type(), new KDevelop_DUContext_imports);
	builtin->insertFunction("KDevelop_DUContext_inDUChain", KDevelop_DUContext_inDUChain::type(), new KDevelop_DUContext_inDUChain);
	builtin->insertFunction("KDevelop_DUContext_createNavigationWidget", KDevelop_DUContext_createNavigationWidget::type(), new KDevelop_DUContext_createNavigationWidget);
	builtin->insertFunction("KDevelop_DUContext_owner", KDevelop_DUContext_owner::type(), new KDevelop_DUContext_owner);
	builtin->insertFunction("KDevelop_DUContext_inSymbolTable", KDevelop_DUContext_inSymbolTable::type(), new KDevelop_DUContext_inSymbolTable);
	builtin->insertFunction("KDevelop_DUContext_indexedLocalScopeIdentifier", KDevelop_DUContext_indexedLocalScopeIdentifier::type(), new KDevelop_DUContext_indexedLocalScopeIdentifier);
	builtin->insertFunction("KDevelop_DUContext_findContextAt", KDevelop_DUContext_findContextAt::type(), new KDevelop_DUContext_findContextAt);
	builtin->insertFunction("KDevelop_DUContext_findDeclarations", KDevelop_DUContext_findDeclarations::type(), new KDevelop_DUContext_findDeclarations);
	builtin->insertFunction("KDevelop_DUContext_findDeclarationAt", KDevelop_DUContext_findDeclarationAt::type(), new KDevelop_DUContext_findDeclarationAt);
	builtin->insertFunction("KDevelop_DUContext_findLocalDeclarations", KDevelop_DUContext_findLocalDeclarations::type(), new KDevelop_DUContext_findLocalDeclarations);
	builtin->insertFunction("KDevelop_DUContext_localScopeIdentifier", KDevelop_DUContext_localScopeIdentifier::type(), new KDevelop_DUContext_localScopeIdentifier);
	builtin->insertFunction("KDevelop_DUContext_childContexts", KDevelop_DUContext_childContexts::type(), new KDevelop_DUContext_childContexts);
	builtin->insertFunction("KDevelop_DUContext_fullyApplyAliases", KDevelop_DUContext_fullyApplyAliases::type(), new KDevelop_DUContext_fullyApplyAliases);
	builtin->insertFunction("KDevelop_DUContext_parentContextOf", KDevelop_DUContext_parentContextOf::type(), new KDevelop_DUContext_parentContextOf);
	builtin->insertFunction("KDevelop_DUContext_topContext", KDevelop_DUContext_topContext::type(), new KDevelop_DUContext_topContext);
	builtin->insertFunction("KDevelop_DUContext_findContextIncluding", KDevelop_DUContext_findContextIncluding::type(), new KDevelop_DUContext_findContextIncluding);
	builtin->insertFunction("KDevelop_DUContext_usesCount", KDevelop_DUContext_usesCount::type(), new KDevelop_DUContext_usesCount);
	builtin->insertFunction("KDevelop_DUContext_equalScopeIdentifier", KDevelop_DUContext_equalScopeIdentifier::type(), new KDevelop_DUContext_equalScopeIdentifier);
	builtin->insertFunction("KDevelop_DUContext_type", KDevelop_DUContext_type::type(), new KDevelop_DUContext_type);
	builtin->insertFunction("KDevelop_DUContext_importedParentContexts", KDevelop_DUContext_importedParentContexts::type(), new KDevelop_DUContext_importedParentContexts);
	builtin->insertFunction("KDevelop_DUContext_scopeIdentifier", KDevelop_DUContext_scopeIdentifier::type(), new KDevelop_DUContext_scopeIdentifier);
	builtin->insertFunction("KDevelop_DUContext_importers", KDevelop_DUContext_importers::type(), new KDevelop_DUContext_importers);
	builtin->insertFunction("KDevelop_DUContext_parentContext", KDevelop_DUContext_parentContext::type(), new KDevelop_DUContext_parentContext);
	builtin->insertFunction("KDevelop_IndexedDUContext_isValid", KDevelop_IndexedDUContext_isValid::type(), new KDevelop_IndexedDUContext_isValid);
	builtin->insertFunction("KDevelop_IndexedDUContext_localIndex", KDevelop_IndexedDUContext_localIndex::type(), new KDevelop_IndexedDUContext_localIndex);
	builtin->insertFunction("KDevelop_IndexedDUContext_dummyData", KDevelop_IndexedDUContext_dummyData::type(), new KDevelop_IndexedDUContext_dummyData);
	builtin->insertFunction("KDevelop_IndexedDUContext_indexedTopContext", KDevelop_IndexedDUContext_indexedTopContext::type(), new KDevelop_IndexedDUContext_indexedTopContext);
	builtin->insertFunction("KDevelop_IndexedDUContext_context", KDevelop_IndexedDUContext_context::type(), new KDevelop_IndexedDUContext_context);
	builtin->insertFunction("KDevelop_IndexedDUContext_data", KDevelop_IndexedDUContext_data::type(), new KDevelop_IndexedDUContext_data);
	builtin->insertFunction("KDevelop_IndexedDUContext_isDummy", KDevelop_IndexedDUContext_isDummy::type(), new KDevelop_IndexedDUContext_isDummy);
	builtin->insertFunction("KDevelop_IndexedDUContext_topContextIndex", KDevelop_IndexedDUContext_topContextIndex::type(), new KDevelop_IndexedDUContext_topContextIndex);
	builtin->insertFunction("KDevelop_IndexedDUContext_hash", KDevelop_IndexedDUContext_hash::type(), new KDevelop_IndexedDUContext_hash);
	variables->modify("KDevelop_AbstractType_TypeAbstract", Cn(KDevelop::AbstractType::TypeAbstract));
	variables->modify("KDevelop_AbstractType_TypeIntegral", Cn(KDevelop::AbstractType::TypeIntegral));
	variables->modify("KDevelop_AbstractType_TypePointer", Cn(KDevelop::AbstractType::TypePointer));
	variables->modify("KDevelop_AbstractType_TypeReference", Cn(KDevelop::AbstractType::TypeReference));
	variables->modify("KDevelop_AbstractType_TypeFunction", Cn(KDevelop::AbstractType::TypeFunction));
	variables->modify("KDevelop_AbstractType_TypeStructure", Cn(KDevelop::AbstractType::TypeStructure));
	variables->modify("KDevelop_AbstractType_TypeArray", Cn(KDevelop::AbstractType::TypeArray));
	variables->modify("KDevelop_AbstractType_TypeDelayed", Cn(KDevelop::AbstractType::TypeDelayed));
	variables->modify("KDevelop_AbstractType_TypeEnumeration", Cn(KDevelop::AbstractType::TypeEnumeration));
	variables->modify("KDevelop_AbstractType_TypeEnumerator", Cn(KDevelop::AbstractType::TypeEnumerator));
	variables->modify("KDevelop_AbstractType_TypeAlias", Cn(KDevelop::AbstractType::TypeAlias));
	variables->modify("KDevelop_AbstractType_TypeUnsure", Cn(KDevelop::AbstractType::TypeUnsure));
	variables->modify("KDevelop_AbstractType_Identity", Cn(KDevelop::AbstractType::Identity));
	variables->modify("KDevelop_AbstractType_NoModifiers", Cn(KDevelop::AbstractType::NoModifiers));
	variables->modify("KDevelop_AbstractType_ConstModifier", Cn(KDevelop::AbstractType::ConstModifier));
	variables->modify("KDevelop_AbstractType_VolatileModifier", Cn(KDevelop::AbstractType::VolatileModifier));
	variables->modify("KDevelop_AbstractType_TransientModifier", Cn(KDevelop::AbstractType::TransientModifier));
	variables->modify("KDevelop_AbstractType_NewModifier", Cn(KDevelop::AbstractType::NewModifier));
	variables->modify("KDevelop_AbstractType_SealedModifier", Cn(KDevelop::AbstractType::SealedModifier));
	variables->modify("KDevelop_AbstractType_UnsafeModifier", Cn(KDevelop::AbstractType::UnsafeModifier));
	variables->modify("KDevelop_AbstractType_FixedModifier", Cn(KDevelop::AbstractType::FixedModifier));
	variables->modify("KDevelop_AbstractType_ShortModifier", Cn(KDevelop::AbstractType::ShortModifier));
	variables->modify("KDevelop_AbstractType_LongModifier", Cn(KDevelop::AbstractType::LongModifier));
	variables->modify("KDevelop_AbstractType_LongLongModifier", Cn(KDevelop::AbstractType::LongLongModifier));
	variables->modify("KDevelop_AbstractType_SignedModifier", Cn(KDevelop::AbstractType::SignedModifier));
	variables->modify("KDevelop_AbstractType_UnsignedModifier", Cn(KDevelop::AbstractType::UnsignedModifier));
	variables->modify("KDevelop_AbstractType_LanguageSpecificModifier", Cn(KDevelop::AbstractType::LanguageSpecificModifier));
	builtin->insertFunction("KDevelop_AbstractType_toString", KDevelop_AbstractType_toString::type(), new KDevelop_AbstractType_toString);
	builtin->insertFunction("KDevelop_AbstractType_clone", KDevelop_AbstractType_clone::type(), new KDevelop_AbstractType_clone);
	builtin->insertFunction("KDevelop_AbstractType_indexed", KDevelop_AbstractType_indexed::type(), new KDevelop_AbstractType_indexed);
	builtin->insertFunction("KDevelop_AbstractType_whichType", KDevelop_AbstractType_whichType::type(), new KDevelop_AbstractType_whichType);
	builtin->insertFunction("KDevelop_AbstractType_equals", KDevelop_AbstractType_equals::type(), new KDevelop_AbstractType_equals);
	builtin->insertFunction("KDevelop_AbstractType_modifiers", KDevelop_AbstractType_modifiers::type(), new KDevelop_AbstractType_modifiers);
	builtin->insertFunction("KDevelop_AbstractType_hash", KDevelop_AbstractType_hash::type(), new KDevelop_AbstractType_hash);
	builtin->insertFunction("KDevelop_ClassFunctionDeclarationToKDevelop_ClassFunctionDeclarationBase", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")).addParameter(ExpressionType("KDevelop::ClassFunctionDeclarationBase*")), new Cast<KDevelop::ClassFunctionDeclaration, KDevelop::ClassFunctionDeclarationBase>);
	builtin->insertFunction("KDevelop_ClassFunctionDeclarationBaseToKDevelop_ClassFunctionDeclaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclarationBase*")).addParameter(ExpressionType("KDevelop::ClassFunctionDeclaration*")), new DownCast<KDevelop::ClassFunctionDeclarationBase, KDevelop::ClassFunctionDeclaration>);
	builtin->insertFunction("KDevelop_ClassFunctionDeclarationBaseIsKDevelop_ClassFunctionDeclaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassFunctionDeclarationBase*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::ClassFunctionDeclarationBase, KDevelop::ClassFunctionDeclaration>);
	variables->modify("KDevelop_ClassFunctionDeclaration_Identity", Cn(KDevelop::ClassFunctionDeclaration::Identity));
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_defaultParametersSize", KDevelop_ClassFunctionDeclaration_defaultParametersSize::type(), new KDevelop_ClassFunctionDeclaration_defaultParametersSize);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_toString", KDevelop_ClassFunctionDeclaration_toString::type(), new KDevelop_ClassFunctionDeclaration_toString);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isConstructor", KDevelop_ClassFunctionDeclaration_isConstructor::type(), new KDevelop_ClassFunctionDeclaration_isConstructor);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isSignal", KDevelop_ClassFunctionDeclaration_isSignal::type(), new KDevelop_ClassFunctionDeclaration_isSignal);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isAbstract", KDevelop_ClassFunctionDeclaration_isAbstract::type(), new KDevelop_ClassFunctionDeclaration_isAbstract);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isFinal", KDevelop_ClassFunctionDeclaration_isFinal::type(), new KDevelop_ClassFunctionDeclaration_isFinal);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isSlot", KDevelop_ClassFunctionDeclaration_isSlot::type(), new KDevelop_ClassFunctionDeclaration_isSlot);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_additionalIdentity", KDevelop_ClassFunctionDeclaration_additionalIdentity::type(), new KDevelop_ClassFunctionDeclaration_additionalIdentity);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isFunctionDeclaration", KDevelop_ClassFunctionDeclaration_isFunctionDeclaration::type(), new KDevelop_ClassFunctionDeclaration_isFunctionDeclaration);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isConversionFunction", KDevelop_ClassFunctionDeclaration_isConversionFunction::type(), new KDevelop_ClassFunctionDeclaration_isConversionFunction);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_isDestructor", KDevelop_ClassFunctionDeclaration_isDestructor::type(), new KDevelop_ClassFunctionDeclaration_isDestructor);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_defaultParameters", KDevelop_ClassFunctionDeclaration_defaultParameters::type(), new KDevelop_ClassFunctionDeclaration_defaultParameters);
	builtin->insertFunction("KDevelop_ClassFunctionDeclaration_clonePrivate", KDevelop_ClassFunctionDeclaration_clonePrivate::type(), new KDevelop_ClassFunctionDeclaration_clonePrivate);
	builtin->insertFunction("KDevelop_ClassMemberDeclarationToKDevelop_Declaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")).addParameter(ExpressionType("KDevelop::Declaration*")), new Cast<KDevelop::ClassMemberDeclaration, KDevelop::Declaration>);
	builtin->insertFunction("KDevelop_DeclarationToKDevelop_ClassMemberDeclaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType("KDevelop::ClassMemberDeclaration*")), new DownCast<KDevelop::Declaration, KDevelop::ClassMemberDeclaration>);
	builtin->insertFunction("KDevelop_DeclarationIsKDevelop_ClassMemberDeclaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Declaration*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::Declaration, KDevelop::ClassMemberDeclaration>);
	variables->modify("KDevelop_ClassMemberDeclaration_StaticSpecifier", Cn(KDevelop::ClassMemberDeclaration::StaticSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_AutoSpecifier", Cn(KDevelop::ClassMemberDeclaration::AutoSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_FriendSpecifier", Cn(KDevelop::ClassMemberDeclaration::FriendSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_ExternSpecifier", Cn(KDevelop::ClassMemberDeclaration::ExternSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_RegisterSpecifier", Cn(KDevelop::ClassMemberDeclaration::RegisterSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_MutableSpecifier", Cn(KDevelop::ClassMemberDeclaration::MutableSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_FinalSpecifier", Cn(KDevelop::ClassMemberDeclaration::FinalSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_NativeSpecifier", Cn(KDevelop::ClassMemberDeclaration::NativeSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_SynchronizedSpecifier", Cn(KDevelop::ClassMemberDeclaration::SynchronizedSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_StrictFPSpecifier", Cn(KDevelop::ClassMemberDeclaration::StrictFPSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_AbstractSpecifier", Cn(KDevelop::ClassMemberDeclaration::AbstractSpecifier));
	variables->modify("KDevelop_ClassMemberDeclaration_Identity", Cn(KDevelop::ClassMemberDeclaration::Identity));
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isSynchronized", KDevelop_ClassMemberDeclaration_isSynchronized::type(), new KDevelop_ClassMemberDeclaration_isSynchronized);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isAbstract", KDevelop_ClassMemberDeclaration_isAbstract::type(), new KDevelop_ClassMemberDeclaration_isAbstract);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isFriend", KDevelop_ClassMemberDeclaration_isFriend::type(), new KDevelop_ClassMemberDeclaration_isFriend);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isStatic", KDevelop_ClassMemberDeclaration_isStatic::type(), new KDevelop_ClassMemberDeclaration_isStatic);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isNative", KDevelop_ClassMemberDeclaration_isNative::type(), new KDevelop_ClassMemberDeclaration_isNative);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isStrictFP", KDevelop_ClassMemberDeclaration_isStrictFP::type(), new KDevelop_ClassMemberDeclaration_isStrictFP);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isMutable", KDevelop_ClassMemberDeclaration_isMutable::type(), new KDevelop_ClassMemberDeclaration_isMutable);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_accessPolicy", KDevelop_ClassMemberDeclaration_accessPolicy::type(), new KDevelop_ClassMemberDeclaration_accessPolicy);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isRegister", KDevelop_ClassMemberDeclaration_isRegister::type(), new KDevelop_ClassMemberDeclaration_isRegister);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isExtern", KDevelop_ClassMemberDeclaration_isExtern::type(), new KDevelop_ClassMemberDeclaration_isExtern);
	builtin->insertFunction("KDevelop_ClassMemberDeclaration_isAuto", KDevelop_ClassMemberDeclaration_isAuto::type(), new KDevelop_ClassMemberDeclaration_isAuto);
	variables->modify("KDevelop_AbstractFunctionDeclaration_VirtualSpecifier", Cn(KDevelop::AbstractFunctionDeclaration::VirtualSpecifier));
	variables->modify("KDevelop_AbstractFunctionDeclaration_InlineSpecifier", Cn(KDevelop::AbstractFunctionDeclaration::InlineSpecifier));
	variables->modify("KDevelop_AbstractFunctionDeclaration_ExplicitSpecifier", Cn(KDevelop::AbstractFunctionDeclaration::ExplicitSpecifier));
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_defaultParameterForArgument", KDevelop_AbstractFunctionDeclaration_defaultParameterForArgument::type(), new KDevelop_AbstractFunctionDeclaration_defaultParameterForArgument);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_defaultParametersSize", KDevelop_AbstractFunctionDeclaration_defaultParametersSize::type(), new KDevelop_AbstractFunctionDeclaration_defaultParametersSize);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_defaultParameters", KDevelop_AbstractFunctionDeclaration_defaultParameters::type(), new KDevelop_AbstractFunctionDeclaration_defaultParameters);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_isExplicit", KDevelop_AbstractFunctionDeclaration_isExplicit::type(), new KDevelop_AbstractFunctionDeclaration_isExplicit);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_internalFunctionContext", KDevelop_AbstractFunctionDeclaration_internalFunctionContext::type(), new KDevelop_AbstractFunctionDeclaration_internalFunctionContext);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_isInline", KDevelop_AbstractFunctionDeclaration_isInline::type(), new KDevelop_AbstractFunctionDeclaration_isInline);
	builtin->insertFunction("KDevelop_AbstractFunctionDeclaration_isVirtual", KDevelop_AbstractFunctionDeclaration_isVirtual::type(), new KDevelop_AbstractFunctionDeclaration_isVirtual);
	builtin->insertFunction("KDevelop_FunctionDefinitionToKDevelop_FunctionDeclaration", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::FunctionDefinition*")).addParameter(ExpressionType("KDevelop::FunctionDeclaration*")), new Cast<KDevelop::FunctionDefinition, KDevelop::FunctionDeclaration>);
	builtin->insertFunction("KDevelop_FunctionDeclarationToKDevelop_FunctionDefinition", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::FunctionDeclaration*")).addParameter(ExpressionType("KDevelop::FunctionDefinition*")), new DownCast<KDevelop::FunctionDeclaration, KDevelop::FunctionDefinition>);
	builtin->insertFunction("KDevelop_FunctionDeclarationIsKDevelop_FunctionDefinition", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::FunctionDeclaration*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::FunctionDeclaration, KDevelop::FunctionDefinition>);
	variables->modify("KDevelop_FunctionDefinition_Identity", Cn(KDevelop::FunctionDefinition::Identity));
	builtin->insertFunction("KDevelop_FunctionDefinition_hasDeclaration", KDevelop_FunctionDefinition_hasDeclaration::type(), new KDevelop_FunctionDefinition_hasDeclaration);
	builtin->insertFunction("KDevelop_FunctionDefinition_declaration", KDevelop_FunctionDefinition_declaration::type(), new KDevelop_FunctionDefinition_declaration);
	variables->modify("KDevelop_DUChainBase_Identity", Cn(KDevelop::DUChainBase::Identity));
	builtin->insertFunction("KDevelop_DUChainBase_topContext", KDevelop_DUChainBase_topContext::type(), new KDevelop_DUChainBase_topContext);
	builtin->insertFunction("KDevelop_DUChainBase_d_func_dynamic", KDevelop_DUChainBase_d_func_dynamic::type(), new KDevelop_DUChainBase_d_func_dynamic);
	builtin->insertFunction("KDevelop_DUChainBase_rangeInCurrentRevision", KDevelop_DUChainBase_rangeInCurrentRevision::type(), new KDevelop_DUChainBase_rangeInCurrentRevision);
	builtin->insertFunction("KDevelop_DUChainBase_range", KDevelop_DUChainBase_range::type(), new KDevelop_DUChainBase_range);
	builtin->insertFunction("KDevelop_DUChainBase_weakPointer", KDevelop_DUChainBase_weakPointer::type(), new KDevelop_DUChainBase_weakPointer);
	builtin->insertFunction("KDevelop_DUChainBase_transformToLocalRevision", KDevelop_DUChainBase_transformToLocalRevision::type(), new KDevelop_DUChainBase_transformToLocalRevision);
	builtin->insertFunction("KDevelop_DUChainBase_createRangeMoving", KDevelop_DUChainBase_createRangeMoving::type(), new KDevelop_DUChainBase_createRangeMoving);
	builtin->insertFunction("KDevelop_DUChainBase_url", KDevelop_DUChainBase_url::type(), new KDevelop_DUChainBase_url);
	builtin->insertFunction("KDevelop_DUChainBase_transformFromLocalRevision", KDevelop_DUChainBase_transformFromLocalRevision::type(), new KDevelop_DUChainBase_transformFromLocalRevision);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_beginsWith", KDevelop_QualifiedIdentifier_beginsWith::type(), new KDevelop_QualifiedIdentifier_beginsWith);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_mid", KDevelop_QualifiedIdentifier_mid::type(), new KDevelop_QualifiedIdentifier_mid);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_first", KDevelop_QualifiedIdentifier_first::type(), new KDevelop_QualifiedIdentifier_first);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_isEmpty", KDevelop_QualifiedIdentifier_isEmpty::type(), new KDevelop_QualifiedIdentifier_isEmpty);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_isExpression", KDevelop_QualifiedIdentifier_isExpression::type(), new KDevelop_QualifiedIdentifier_isExpression);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_toStringList", KDevelop_QualifiedIdentifier_toStringList::type(), new KDevelop_QualifiedIdentifier_toStringList);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_inRepository", KDevelop_QualifiedIdentifier_inRepository::type(), new KDevelop_QualifiedIdentifier_inRepository);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_last", KDevelop_QualifiedIdentifier_last::type(), new KDevelop_QualifiedIdentifier_last);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_top", KDevelop_QualifiedIdentifier_top::type(), new KDevelop_QualifiedIdentifier_top);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_index", KDevelop_QualifiedIdentifier_index::type(), new KDevelop_QualifiedIdentifier_index);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_toString", KDevelop_QualifiedIdentifier_toString::type(), new KDevelop_QualifiedIdentifier_toString);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_left", KDevelop_QualifiedIdentifier_left::type(), new KDevelop_QualifiedIdentifier_left);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_merge", KDevelop_QualifiedIdentifier_merge::type(), new KDevelop_QualifiedIdentifier_merge);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_hash", KDevelop_QualifiedIdentifier_hash::type(), new KDevelop_QualifiedIdentifier_hash);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_explicitlyGlobal", KDevelop_QualifiedIdentifier_explicitlyGlobal::type(), new KDevelop_QualifiedIdentifier_explicitlyGlobal);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_count", KDevelop_QualifiedIdentifier_count::type(), new KDevelop_QualifiedIdentifier_count);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_isQualified", KDevelop_QualifiedIdentifier_isQualified::type(), new KDevelop_QualifiedIdentifier_isQualified);
	builtin->insertFunction("KDevelop_QualifiedIdentifier_at", KDevelop_QualifiedIdentifier_at::type(), new KDevelop_QualifiedIdentifier_at);
	builtin->insertFunction("KDevelop_ProblemToKDevelop_DUChainBase", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::Problem*")).addParameter(ExpressionType("KDevelop::DUChainBase*")), new Cast<KDevelop::Problem, KDevelop::DUChainBase>);
	builtin->insertFunction("KDevelop_DUChainBaseToKDevelop_Problem", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType("KDevelop::Problem*")), new DownCast<KDevelop::DUChainBase, KDevelop::Problem>);
	builtin->insertFunction("KDevelop_DUChainBaseIsKDevelop_Problem", ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUChainBase*")).addParameter(ExpressionType::Bool), new CanCast<KDevelop::DUChainBase, KDevelop::Problem>);
	variables->modify("KDevelop_Problem_Identity", Cn(KDevelop::Problem::Identity));
	builtin->insertFunction("KDevelop_Problem_source", KDevelop_Problem_source::type(), new KDevelop_Problem_source);
	builtin->insertFunction("KDevelop_Problem_severity", KDevelop_Problem_severity::type(), new KDevelop_Problem_severity);
	builtin->insertFunction("KDevelop_Problem_toString", KDevelop_Problem_toString::type(), new KDevelop_Problem_toString);
	builtin->insertFunction("KDevelop_Problem_description", KDevelop_Problem_description::type(), new KDevelop_Problem_description);
	builtin->insertFunction("KDevelop_Problem_solutionAssistant", KDevelop_Problem_solutionAssistant::type(), new KDevelop_Problem_solutionAssistant);
	builtin->insertFunction("KDevelop_Problem_finalLocation", KDevelop_Problem_finalLocation::type(), new KDevelop_Problem_finalLocation);
	builtin->insertFunction("KDevelop_Problem_sourceString", KDevelop_Problem_sourceString::type(), new KDevelop_Problem_sourceString);
	builtin->insertFunction("KDevelop_Problem_url", KDevelop_Problem_url::type(), new KDevelop_Problem_url);
	builtin->insertFunction("KDevelop_Problem_locationStack", KDevelop_Problem_locationStack::type(), new KDevelop_Problem_locationStack);
	builtin->insertFunction("KDevelop_Problem_explanation", KDevelop_Problem_explanation::type(), new KDevelop_Problem_explanation);
	builtin->insertFunction("KDevelop_CheckData_access", KDevelop_CheckData_access::type(), new KDevelop_CheckData_access);
	builtin->insertFunction("KDevelop_CheckData_url", KDevelop_CheckData_url::type(), new KDevelop_CheckData_url);
	builtin->insertFunction("KDevelop_CheckData_flow", KDevelop_CheckData_flow::type(), new KDevelop_CheckData_flow);
	builtin->insertFunction("KDevelop_CheckData_top", KDevelop_CheckData_top::type(), new KDevelop_CheckData_top);
	variables->modify("KDevelop_ControlFlowNode_Conditional", Cn(KDevelop::ControlFlowNode::Conditional));
	variables->modify("KDevelop_ControlFlowNode_Sequential", Cn(KDevelop::ControlFlowNode::Sequential));
	variables->modify("KDevelop_ControlFlowNode_Exit", Cn(KDevelop::ControlFlowNode::Exit));
	builtin->insertFunction("KDevelop_ControlFlowNode_conditionRange", KDevelop_ControlFlowNode_conditionRange::type(), new KDevelop_ControlFlowNode_conditionRange);
	builtin->insertFunction("KDevelop_ControlFlowNode_alternative", KDevelop_ControlFlowNode_alternative::type(), new KDevelop_ControlFlowNode_alternative);
	builtin->insertFunction("KDevelop_ControlFlowNode_next", KDevelop_ControlFlowNode_next::type(), new KDevelop_ControlFlowNode_next);
	builtin->insertFunction("KDevelop_ControlFlowNode_nodeRange", KDevelop_ControlFlowNode_nodeRange::type(), new KDevelop_ControlFlowNode_nodeRange);
	builtin->insertFunction("KDevelop_ControlFlowNode_type", KDevelop_ControlFlowNode_type::type(), new KDevelop_ControlFlowNode_type);
	builtin->insertFunction("KDevelop_ControlFlowGraph_rootNodes", KDevelop_ControlFlowGraph_rootNodes::type(), new KDevelop_ControlFlowGraph_rootNodes);
	builtin->insertFunction("KDevelop_ControlFlowGraph_deadNodes", KDevelop_ControlFlowGraph_deadNodes::type(), new KDevelop_ControlFlowGraph_deadNodes);
	builtin->insertFunction("KDevelop_ControlFlowGraph_nodeForDeclaration", KDevelop_ControlFlowGraph_nodeForDeclaration::type(), new KDevelop_ControlFlowGraph_nodeForDeclaration);
	builtin->insertFunction("KDevelop_ControlFlowGraph_declarations", KDevelop_ControlFlowGraph_declarations::type(), new KDevelop_ControlFlowGraph_declarations);
	variables->modify("KDevelop_DataAccess_None", Cn(KDevelop::DataAccess::None));
	variables->modify("KDevelop_DataAccess_Read", Cn(KDevelop::DataAccess::Read));
	variables->modify("KDevelop_DataAccess_Write", Cn(KDevelop::DataAccess::Write));
	variables->modify("KDevelop_DataAccess_Call", Cn(KDevelop::DataAccess::Call));
	builtin->insertFunction("KDevelop_DataAccess_flags", KDevelop_DataAccess_flags::type(), new KDevelop_DataAccess_flags);
	builtin->insertFunction("KDevelop_DataAccess_isCall", KDevelop_DataAccess_isCall::type(), new KDevelop_DataAccess_isCall);
	builtin->insertFunction("KDevelop_DataAccess_value", KDevelop_DataAccess_value::type(), new KDevelop_DataAccess_value);
	builtin->insertFunction("KDevelop_DataAccess_pos", KDevelop_DataAccess_pos::type(), new KDevelop_DataAccess_pos);
	builtin->insertFunction("KDevelop_DataAccess_isRead", KDevelop_DataAccess_isRead::type(), new KDevelop_DataAccess_isRead);
	builtin->insertFunction("KDevelop_DataAccess_isWrite", KDevelop_DataAccess_isWrite::type(), new KDevelop_DataAccess_isWrite);
	builtin->insertFunction("KDevelop_DataAccessRepository_modifications", KDevelop_DataAccessRepository_modifications::type(), new KDevelop_DataAccessRepository_modifications);
	builtin->insertFunction("KDevelop_DataAccessRepository_accessesInRange", KDevelop_DataAccessRepository_accessesInRange::type(), new KDevelop_DataAccessRepository_accessesInRange);
	builtin->insertFunction("KDevelop_DataAccessRepository_accessAt", KDevelop_DataAccessRepository_accessAt::type(), new KDevelop_DataAccessRepository_accessAt);
	builtin->insertFunction("refImport",
		ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::DUContext::Import")).addParameter(ExpressionType("KDevelop::DUContext::Import*")),
		new RefQVariantType<KDevelop::DUContext::Import>);
	builtin->insertFunction("refIndexedDUContext",
		ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::IndexedDUContext")).addParameter(ExpressionType("KDevelop::IndexedDUContext*")),
		new RefQVariantType<KDevelop::IndexedDUContext>);
	builtin->insertFunction("refQualifiedIdentifier",
		ExpressionType(ExpressionType::Lambda).addParameter(ExpressionType("KDevelop::QualifiedIdentifier")).addParameter(ExpressionType("KDevelop::QualifiedIdentifier*")),
		new RefQVariantType<KDevelop::QualifiedIdentifier>);
	builtin->insertFunction("usesForCtx", Helpers_usesForCtx::type(), new Helpers_usesForCtx);
	builtin->insertFunction("canGo", Helpers_canGo::type(), new Helpers_canGo);
	builtin->insertFunction("rangeIsValid", Helpers_rangeIsValid::type(), new Helpers_rangeIsValid);
	builtin->insertFunction("Problem", Helpers_Problem::type(), new Helpers_Problem);
	builtin->insertFunction("allConditionNodes", Helpers_allConditionNodes::type(), new Helpers_allConditionNodes);
	builtin->insertFunction("canGoExt", Helpers_canGoExt::type(), new Helpers_canGoExt);
	builtin->insertFunction("rangeIsEmpty", Helpers_rangeIsEmpty::type(), new Helpers_rangeIsEmpty);
	builtin->insertFunction("invalidCursor", Helpers_invalidCursor::type(), new Helpers_invalidCursor);
	builtin->insertFunction("rangeStart", Helpers_rangeStart::type(), new Helpers_rangeStart);
	builtin->insertFunction("rangeToString", Helpers_rangeToString::type(), new Helpers_rangeToString);
	builtin->insertFunction("indexedStringToKUrl", Helpers_indexedStringToKUrl::type(), new Helpers_indexedStringToKUrl);
	builtin->insertFunction("cursorToString", Helpers_cursorToString::type(), new Helpers_cursorToString);
	builtin->insertFunction("cursorIsValid", Helpers_cursorIsValid::type(), new Helpers_cursorIsValid);
	builtin->insertFunction("rangeContains", Helpers_rangeContains::type(), new Helpers_rangeContains);
	builtin->insertFunction("cursorLine", Helpers_cursorLine::type(), new Helpers_cursorLine);
	builtin->insertFunction("useRangesForDeclaration", Helpers_useRangesForDeclaration::type(), new Helpers_useRangesForDeclaration);
	builtin->insertFunction("nodeAt", Helpers_nodeAt::type(), new Helpers_nodeAt);
	builtin->insertFunction("bitAnd", Helpers_bitAnd::type(), new Helpers_bitAnd);
	builtin->insertFunction("rangeEnd", Helpers_rangeEnd::type(), new Helpers_rangeEnd);
	builtin->insertFunction("usesIn", Helpers_usesIn::type(), new Helpers_usesIn);
	builtin->insertFunction("usesInRange", Helpers_usesInRange::type(), new Helpers_usesInRange);
	builtin->insertFunction("cursorColumn", Helpers_cursorColumn::type(), new Helpers_cursorColumn);
	builtin->insertFunction("KDevelop_DeclarationToKDevelop_FunctionDefinition", Helpers_KDevelop_DeclarationToKDevelop_FunctionDefinition::type(), new Helpers_KDevelop_DeclarationToKDevelop_FunctionDefinition);
	builtin->insertFunction("RangeInRevision", Helpers_RangeInRevision::type(), new Helpers_RangeInRevision);
	builtin->insertFunction("KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration", Helpers_KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration::type(), new Helpers_KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration);
	builtin->insertFunction("imported", Helpers_imported::type(), new Helpers_imported);
	builtin->insertFunction("indexedStringToString", Helpers_indexedStringToString::type(), new Helpers_indexedStringToString);
	builtin->insertFunction("KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration", Helpers_KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration::type(), new Helpers_KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration);
	builtin->insertFunction("identifierToString", Helpers_identifierToString::type(), new Helpers_identifierToString);
	builtin->insertFunction("KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration", Helpers_KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration::type(), new Helpers_KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration);
	builtin->insertFunction("useForIndex", Helpers_useForIndex::type(), new Helpers_useForIndex);
	builtin->insertFunction("fromIndex", Helpers_fromIndex::type(), new Helpers_fromIndex);
	builtin->insertFunction("KDevelop_DeclarationIsKDevelop_FunctionDefinition", Helpers_KDevelop_DeclarationIsKDevelop_FunctionDefinition::type(), new Helpers_KDevelop_DeclarationIsKDevelop_FunctionDefinition);

}

