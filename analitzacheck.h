/* This file is part of KDevelop
    Copyright 2011 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef ANALITZACHECK_H
#define ANALITZACHECK_H

#include <interfaces/ilanguagecheck.h>
#include <analitza/expression.h>
#include <analitza/analyzer.h>
#include <analitza/variables.h>
#include <KPluginInfo>
#include <QMutex>

class AnalitzaCheck : public KDevelop::ILanguageCheck
{
	public:
		AnalitzaCheck();
		AnalitzaCheck(const QString& path);
		
		virtual QString name() const;
		virtual void runCheck(const KDevelop::CheckData& data);
		Analitza::Analyzer* analyzer() { return &a; }
		
		void create();
		static int area;
		static QStringList listChecks();
	private:
		void setup();
		bool m_isSetup;
		QString m_path;
		
		Analitza::Analyzer a;
		KPluginInfo m_plugin;
		QMutex m_mutex;
};

#endif // ANALITZACHECK_H
