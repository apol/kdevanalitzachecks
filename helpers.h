/* This file is part of KDevelop
    Copyright 2010 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef HELPERS_H
#define HELPERS_H
#include <language/interfaces/iproblem.h>
#include <language/duchain/ducontext.h>
#include <language/duchain/topducontext.h>
#include <language/duchain/functiondeclaration.h>
#include <language/duchain/functiondefinition.h>

namespace KDevelop {
	class ClassMemberDeclaration;
	class ClassFunctionDeclaration;
	class ControlFlowNode;
	class ControlFlowGraph;
}

namespace Helpers
{
	KDevelop::ProblemPointer Problem(const KDevelop::RangeInRevision& range, const QString& message);
	KDevelop::RangeInRevision RangeInRevision(const KDevelop::CursorInRevision& c1, const KDevelop::CursorInRevision& c2);
	KDevelop::CursorInRevision invalidCursor();
	
	QList< KDevelop::DUContext::Import > imported(KDevelop::TopDUContext* ctx);
	
	QList<KDevelop::Use*> usesForCtx(const KDevelop::DUContext* ctx);
	QList<KDevelop::Use*> usesIn(const KDevelop::TopDUContext* ctx);
	QList<KDevelop::Use*> usesInRange(const KDevelop::DUContext* ctx, const KDevelop::RangeInRevision& range);
	QList<KDevelop::RangeInRevision> useRangesForDeclaration(const KDevelop::Declaration* decl, const KUrl& url);
	KDevelop::Use* useForIndex(KDevelop::DUContext* ctx, int position);
	
	KUrl indexedStringToKUrl(const IndexedString& str);
	QString indexedStringToString(const IndexedString& str);
	
	KDevelop::TopDUContext* fromIndex(uint idxtdc);
	
	KDevelop::CursorInRevision rangeStart(KDevelop::RangeInRevision& range);
	KDevelop::CursorInRevision rangeEnd(KDevelop::RangeInRevision& range);
	bool rangeIsValid(const KDevelop::RangeInRevision& range);
	bool rangeIsEmpty(const KDevelop::RangeInRevision& range);
	bool rangeContains(const KDevelop::RangeInRevision& range, const KDevelop::CursorInRevision& cursor);
	QString rangeToString(const KDevelop::RangeInRevision& range);
	
	int cursorLine(KDevelop::CursorInRevision& c);
	int cursorColumn(KDevelop::CursorInRevision& c);
	bool cursorIsValid(KDevelop::CursorInRevision& c);
	QString cursorToString(const KDevelop::CursorInRevision& c);
	
	QList<KDevelop::ControlFlowNode*> allConditionNodes(const QList<ControlFlowNode*>& nodes);
	
	bool bitAnd(int a, int b);
	
	KDevelop::AbstractFunctionDeclaration* KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration(KDevelop::ClassFunctionDeclaration* decl);
	KDevelop::ClassFunctionDeclaration* KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration(KDevelop::ClassMemberDeclaration* decl);
	bool KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration(KDevelop::ClassMemberDeclaration* decl);
	
	KDevelop::FunctionDefinition* KDevelop_DeclarationToKDevelop_FunctionDefinition(KDevelop::Declaration* decl);
	bool KDevelop_DeclarationIsKDevelop_FunctionDefinition(KDevelop::Declaration* decl);
	
	bool canGo(KDevelop::ControlFlowNode* a, KDevelop::ControlFlowNode* b);
	bool canGoExt(KDevelop::ControlFlowNode* a, KDevelop::ControlFlowNode* b, const QList<KDevelop::CursorInRevision>& except);
	
	ControlFlowNode* nodeAt(KDevelop::ControlFlowGraph* graph, const KDevelop::CursorInRevision& cursor);
	QString identifierToString(const KDevelop::Identifier& id);
}

#endif // HELPERS_H
