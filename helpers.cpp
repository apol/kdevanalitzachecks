/* This file is part of KDevelop
    Copyright 2010 Aleix Pol Gonzalez <aleixpol@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "helpers.h"
#include <language/duchain/topducontext.h>
#include <language/duchain/use.h>
#include <language/duchain/declaration.h>
#include <language/duchain/classmemberdeclaration.h>
#include <language/duchain/classfunctiondeclaration.h>
#include <language/checks/controlflownode.h>
#include <language/checks/controlflowgraph.h>

Q_DECLARE_METATYPE(KDevelop::DUContext::Import*);

KDevelop::ProblemPointer Helpers::Problem(const KDevelop::RangeInRevision& range, const QString& message)
{
	KDevelop::ProblemPointer ptr(new KDevelop::Problem());
	
	ptr->setRange(range);
	ptr->setDescription(message);
	ptr->setSource(ProblemData::SemanticAnalysis);
	return ptr;
}

KDevelop::RangeInRevision Helpers::RangeInRevision(const KDevelop::CursorInRevision& c1, const KDevelop::CursorInRevision& c2)
{
	return KDevelop::RangeInRevision(c1, c2);
}

bool Helpers::rangeIsValid(const KDevelop::RangeInRevision& range) { return range.isValid(); }
bool Helpers::rangeIsEmpty(const KDevelop::RangeInRevision& range) { return range.isEmpty(); }
bool Helpers::rangeContains(const KDevelop::RangeInRevision& range, const CursorInRevision& cursor) { return range.contains(cursor); }

QString Helpers::rangeToString(const KDevelop::RangeInRevision& range)
{
	return QString("[%1 %2]").arg(cursorToString(range.start)).arg(cursorToString(range.end));
}

QString Helpers::cursorToString(const CursorInRevision& c)
{
	return QString("(%1,%2)").arg(c.line).arg(c.column);
}


KDevelop::CursorInRevision Helpers::invalidCursor()
{
	return KDevelop::CursorInRevision::invalid();
}

QList< KDevelop::DUContext::Import > Helpers::imported(KDevelop::TopDUContext* ctx)
{
	return ctx->importedParentContexts().toList();
}

QList<KDevelop::Use*> Helpers::usesForCtx(const KDevelop::DUContext* ctx)
{
	QList<KDevelop::Use*> ret;
	for(int i=0; i<ctx->usesCount(); ++i) {
		ret += const_cast<KDevelop::Use*>(&ctx->uses()[i]);
	}
	
	foreach(const KDevelop::DUContext* child, ctx->childContexts()) {
		ret += usesForCtx(child);
	}
	
	return ret;
}

QList< KDevelop::Use* > Helpers::usesIn(const KDevelop::TopDUContext* ctx)
{
	return usesForCtx(ctx);
}

KDevelop::TopDUContext* Helpers::fromIndex(uint idxtdc)
{
	KDevelop::IndexedTopDUContext top(idxtdc);
	return top.data();
}

KDevelop::CursorInRevision Helpers::rangeEnd(KDevelop::RangeInRevision& range) { return range.end; }
KDevelop::CursorInRevision Helpers::rangeStart(KDevelop::RangeInRevision& range) { return range.start; }

int Helpers::cursorColumn(CursorInRevision& c) { return c.column; }
int Helpers::cursorLine(CursorInRevision& c) { return c.line; }

bool Helpers::cursorIsValid(CursorInRevision& c) { return c.isValid(); }

bool Helpers::bitAnd(int a, int b)
{
	return a&b;
}

QList< KDevelop::RangeInRevision > Helpers::useRangesForDeclaration(const KDevelop::Declaration* decl, const KUrl& url)
{
	return decl->uses().value(KDevelop::IndexedString(url));
}

KDevelop::Use* Helpers::useForIndex(KDevelop::DUContext* ctx, int position)
{
	return const_cast<KDevelop::Use*>(&ctx->uses()[position]);
}

QList< KDevelop::Use* > Helpers::usesInRange(const KDevelop::DUContext* ctx, const KDevelop::RangeInRevision& range)
{
	Q_ASSERT(range.isValid());
	
	KDevelop::DUContext* rctx=ctx->findContextAt(range.start);
	Q_ASSERT(rctx);
// 	Q_ASSERT(rctx==ctx->findContextAt(range.end));
	
	QList<KDevelop::Use*> ret;
	const KDevelop::Use* uses = rctx->uses();
	for(int i=0, end=rctx->usesCount(); i<end; ++i) {
		if(range.contains(uses[i].m_range))
			ret += const_cast<KDevelop::Use*>(&uses[i]);
	}
	
// 	qSort(ret.begin(), ret.end());
// 	qDebug() << "fefefe" << range << ret;
	return ret;
}

QSet<ControlFlowNode*> conditionNodesInternal(ControlFlowNode* node, QSet<ControlFlowNode*>& visited)
{
	QSet<ControlFlowNode*> ret;
	if(!visited.contains(node) && node->type()!=ControlFlowNode::Exit) {
		visited.insert(node);
		ret = conditionNodesInternal(node->next(), visited);
		
		if(node->type()==ControlFlowNode::Conditional) {
			ret += node;
			ret += conditionNodesInternal(node->alternative(), visited);
		}
	}
	
	return ret;
}

QList<ControlFlowNode*> Helpers::allConditionNodes(const QList<ControlFlowNode*>& nodes)
{
	QList<ControlFlowNode*> ret;
	
	foreach(ControlFlowNode* node, nodes) {
		QSet<ControlFlowNode*> visited;
		ret += conditionNodesInternal(node, visited).toList();
	}
	
	return ret;
}

KUrl Helpers::indexedStringToKUrl(const KDevelop::IndexedString& str)
{
	return str.toUrl();
}

QString Helpers::indexedStringToString(const KDevelop::IndexedString& str)
{
	return str.str();
}

KDevelop::AbstractFunctionDeclaration* Helpers::KDevelop_ClassFunctionDeclarationToKDevelop_AbstractFunctionDeclaration(KDevelop::ClassFunctionDeclaration* decl)
{
	Q_ASSERT(decl);
	return dynamic_cast<KDevelop::AbstractFunctionDeclaration*>(decl);
}

KDevelop::ClassFunctionDeclaration* Helpers::KDevelop_ClassMemberDeclarationToKDevelop_ClassFunctionDeclaration(KDevelop::ClassMemberDeclaration* decl) { return static_cast<KDevelop::ClassFunctionDeclaration*>(decl); }
KDevelop::FunctionDefinition* Helpers::KDevelop_DeclarationToKDevelop_FunctionDefinition(KDevelop::Declaration* decl) { return static_cast<KDevelop::FunctionDefinition*>(decl); }

bool Helpers::KDevelop_ClassMemberDeclarationIsKDevelop_ClassFunctionDeclaration(KDevelop::ClassMemberDeclaration* decl) { return dynamic_cast<KDevelop::ClassFunctionDeclaration*>(decl)!=0; }
bool Helpers::KDevelop_DeclarationIsKDevelop_FunctionDefinition(KDevelop::Declaration* decl) { return dynamic_cast<KDevelop::FunctionDefinition*>(decl)!=0;}

bool canGoInternal(ControlFlowNode* a, ControlFlowNode* b, const QList< CursorInRevision >& except, QSet<ControlFlowNode*>& visited)
{
	if(!a || visited.contains(a)) return false;
	if(a==b) return true;
	
	visited.insert(a);
	
// 	qDebug() << "visited" << a->nodeRange();
	foreach(const CursorInRevision& cur, except) {
		if(a->nodeRange().contains(cur))
			return false;
	}
	
	return canGoInternal(a->next(), b, except, visited) || canGoInternal(a->alternative(), b, except, visited);
}

bool Helpers::canGo(ControlFlowNode* a, ControlFlowNode* b)
{
	QSet<ControlFlowNode*> visited;
	return canGoInternal(a,b, QList<CursorInRevision>(), visited);
}

bool Helpers::canGoExt(ControlFlowNode* a, ControlFlowNode* b, const QList< CursorInRevision >& except)
{
	QSet<ControlFlowNode*> visited;
	bool bb=canGoInternal(a,b, except, visited);
// 	qDebug() << "suuuuu" << a << b << except << visited << bb;
	return bb;
}

ControlFlowNode* nodeAtInternal(ControlFlowNode* n, const CursorInRevision& cursor, QSet<ControlFlowNode*>& visited)
{
	if(!n || visited.contains(n))
		return 0;
	
	visited.insert(n);
	
	if(n->nodeRange().contains(cursor))
		return n;
	
	ControlFlowNode* ret = nodeAtInternal(n->next(), cursor, visited);
	if(ret)
		return ret;
	
	return nodeAtInternal(n->alternative(), cursor, visited);
}

ControlFlowNode* Helpers::nodeAt(ControlFlowGraph* graph, const CursorInRevision& cursor)
{
	QList<Declaration*> declarations = graph->declarations();
	QSet<ControlFlowNode*> done;
	
	foreach(Declaration* d, declarations) {
		if(d->internalContext()->range().contains(cursor)) {
			QSet<ControlFlowNode*> visited;
			
			return nodeAtInternal(graph->nodeForDeclaration(d), cursor, visited);
		}
	}
	
	foreach(ControlFlowNode* x, graph->rootNodes()) {
		QSet<ControlFlowNode*> visited;
		ControlFlowNode* n=nodeAtInternal(x, cursor, visited);
		if(n)
			return n;
	}
	
	return 0;
}

QString Helpers::identifierToString(const Identifier& id)
{
	return id.toString();
}
