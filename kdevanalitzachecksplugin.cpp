/*  This file is part of KDevelop
    Copyright 2011 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "kdevanalitzachecksplugin.h"
#include <KPluginFactory>
#include <KAboutData>
#include <KStandardDirs>
#include <QDir>
#include "analitzacheck.h"
#include <kdebug.h>

K_PLUGIN_FACTORY(KDevAnalitzaChecksFactory, registerPlugin<KDevAnalitzaChecksPlugin>(); )
K_EXPORT_PLUGIN(KDevAnalitzaChecksFactory(
    KAboutData("kdevanalitzachecks","kdevanalitzachecks",
               ki18n("KDevelop Analitza Checks"), "0.1", ki18n("Provides different checks to analyze your code"), KAboutData::License_GPL)))

using namespace KDevelop;

KDevAnalitzaChecksPlugin::KDevAnalitzaChecksPlugin(QObject* parent, const QVariantList&)
    : KDevelop::IPlugin(KDevAnalitzaChecksFactory::componentData(), parent)
{
	AnalitzaCheck::area=KDebug::registerArea("kdevanalitzachecks");
	
    KDEV_USE_EXTENSION_INTERFACE( KDevelop::ILanguageCheckProvider )
}

QList< ILanguageCheck* > KDevAnalitzaChecksPlugin::providedChecks()
{
	if(m_checks.isEmpty()) {
		foreach(const QString& check, AnalitzaCheck::listChecks()) {
			m_checks += new AnalitzaCheck(check);
		}
	}
	
	return m_checks;
}

void KDevAnalitzaChecksPlugin::initializeChecks(const QString& dir)
{
	QDir d(dir);
	QStringList checks = d.entryList(QStringList("*.kdevcheck"));
	
	qDebug() << "analitza checks" << checks;
	foreach(const QString& check, checks) {
		m_checks += new AnalitzaCheck(d.absoluteFilePath(check));
	}
}
