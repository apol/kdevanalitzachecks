/*************************************************************************************
 *  Copyright (C) 2009 by Aleix Pol <aleixpol@kde.org>                               *
 *                                                                                   *
 *  This program is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU General Public License                      *
 *  as published by the Free Software Foundation; either version 2                   *
 *  of the License, or (at your option) any later version.                           *
 *                                                                                   *
 *  This program is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 *  GNU General Public License for more details.                                     *
 *                                                                                   *
 *  You should have received a copy of the GNU General Public License                *
 *  along with this program; if not, write to the Free Software                      *
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA   *
 *************************************************************************************/

#include <KApplication>
#include <KLocale>
#include <KAboutData>
#include <KCmdLineArgs>
#include <KStandardDirs>
#include "analitzacheck.h"
#include <QtCore/QFile>
#include <QDir>
#include <QTextDocument>
#include <analitza/analitzautils.h>

using namespace Analitza;

QString apikdeorgUrl(const QString& name)
{
	return name;
}

QString typeToHtml(const ExpressionType& t)
{
	QString ret;
	switch(t.type()) {
		case ExpressionType::Value:ret="num";  break;
		case ExpressionType::Char: ret="char"; break;
		case ExpressionType::Bool: ret="bool"; break;
		case ExpressionType::List:
			ret="<b>[</b>"+typeToHtml(t.contained())+"<b>]</b>";
			break;
		case ExpressionType::Matrix:
			ret="<b>[</b>"+typeToHtml(t.contained())+"<b>]</b>";
			break;
		case ExpressionType::Vector:
			ret="<b>&lt;</b>"+typeToHtml(t.contained())+','+QString::number(t.size())+"<b>&gt;</b>";
			break;
		case ExpressionType::Error:
			ret="error";
			break;
		case ExpressionType::Lambda: {
			QStringList args;
			foreach(const ExpressionType& arg, t.parameters())
				args += typeToHtml(arg);
			
			ret=args.join("<br/>&nbsp;&nbsp;<b>-&gt;</b> ");
		}	break;
		case ExpressionType::Any:
			ret=t.toString();
			break;
		case ExpressionType::Many: {
			QStringList args;
			foreach(const ExpressionType& arg, t.alternatives())
				args += typeToHtml(arg);
			
			ret=/*"{"+*/args.join(" <b>|</b> ")/*+"}"*/;
		}	break;
		case ExpressionType::Object:
			static const int CUT_SIZE = 50;
			ret="<em>obj:</em>"+Qt::escape(t.objectName());
			if(ret.size()>CUT_SIZE && ret.contains("&lt;")) {
				int l=ret.indexOf("&lt;")+4;
				int last = ret.lastIndexOf("&gt;");
				ret = ret.left(l)+"<span title='"+Qt::escape(t.objectName())+"'>...</span>"+ret.right(ret.size()-last);
			}
			break;
	}
	
	return ret;
}

void genDoc(QTextStream* stream, Analitza::BuiltinMethods* methods)
{
	Q_ASSERT(!methods->varTypes().isEmpty());
	QMap<QString, ExpressionType> data = methods->varTypes();
	for(QMap<QString, ExpressionType>::const_iterator it=data.constBegin(), itEnd=data.constEnd(); it!=itEnd; ++it) {
		
		*stream << "\t<tr>";
		*stream << "<td>"+it.key()+"</td>";
		*stream << "<td>"+typeToHtml(*it)+"</td>";
		if(it.key().contains('_'))
			*stream << "<td><a href='"+apikdeorgUrl(it.key())+"'>"+apikdeorgUrl(it.key())+"</a></td>";
		*stream << "</tr>\n";
	}
}

bool check(const QString& path, bool variables, const QString& dotFile)
{
	QFileInfo pathInfo(path);
	QString res;
	if(pathInfo.exists()) {
		res = pathInfo.absoluteFilePath();
	} else {
		res = KGlobal::dirs()->findResource("data", "kdevanalitzachecks/"+path);
		if(res.isEmpty()) {
			qDebug() << "error: " << path << "not found";
			return false;
		}
	}
	
	bool ret = true;
	AnalitzaCheck a(res);
	a.create();
	if(a.analyzer()->isCorrect()) {
		a.analyzer()->setExpression(Expression("check"));
		Analitza::ExpressionType t=a.analyzer()->type();
		ExpressionType testType=ExpressionType(ExpressionType::Lambda)
			.addParameter(ExpressionType("KDevelop::CheckData*"))
			.addParameter(ExpressionType(ExpressionType::List, ExpressionType("KSharedPtr<KDevelop::Problem>")));
		
		if(!t.canReduceTo(testType)) {
			qDebug() << "Wrong type:" << t.simplifyStars().toString() << "should be " << testType.toString();
			ret = false;
		}
	} else {
		qDebug() << res;
		qDebug() << qPrintable(a.name()) << "Errors:\n -" << qPrintable(a.analyzer()->errors().join("\n - "));
		ret = false;
	}

	if(ret) {
		qDebug() << "GOOD!" << qPrintable(res);
	}

	
	if(variables) {
		QStringList varnames=a.analyzer()->variables()->keys();
		varnames.sort();
		
		Variables* vars=a.analyzer()->variables();
		QMap<QString, ExpressionType> varsType=a.analyzer()->variableTypes();
		foreach(const QString& s, varnames) {
			qDebug() << qPrintable(s) << "=" << vars->value(s)->toString() << "\n -    " << varsType.value(s).toString();
		}
	}
	
	if(!dotFile.isEmpty()) {
		Variables* v=a.analyzer()->variables();
		
		QFile f(dotFile);
		bool o = f.open(QFile::Text | QFile::WriteOnly);
		Q_ASSERT(o);
		QTextStream s(&f);
		
		s << AnalitzaUtils::generateDependencyGraph(v);
	}
	return ret;
}

int main(int argc, char* argv[])
{
	KAboutData aboutData( "kdevanalitzacheckscompiler", 0, ki18n( "kdevanalitzacheckscompiler" ),
						"0.1 alpha", ki18n("Static checks compiler" ), KAboutData::License_GPL_V3,
						ki18n( "Copyright 2011, Aleix Pol Gonzalez" ), KLocalizedString(),
						"http://www.proli.net" );
	
	KCmdLineArgs::init( argc, argv, &aboutData );
	KCmdLineOptions options;
	options.add("+file", ki18n( "Check to load" ));
	options.add("variables", ki18n( "Prints all check's variables" ));
	options.add("doc <filename>", ki18n("Write back some documentation"), "bindings.html");
	options.add("graph <filename>", ki18n("Write back a dot graph file"), "check.dot");
	options.add("list", ki18n("List all analitza checks"));
	KCmdLineArgs::addCmdLineOptions( options );
	KCmdLineArgs* args = KCmdLineArgs::parsedArgs();
	KApplication app;
	
	for(int i=0; i<args->count(); ++i) {
		bool ret = check(args->arg(i), args->isSet("variables"), args->getOption("graph"));
		if(!ret)
			break;
	}
	
	if(args->isSet("doc")) {
		AnalitzaCheck a;
		
		QFile f(args->getOption("doc"));
		if(!f.open(QFile::WriteOnly | QFile::Text)) {
			qDebug("Could not write the documentation file.");
			return 1;
		}
		
		QTextStream text(&f);
		text << "<html>";
		text << "<body>";
		text << "<table>\n";
		text << "<tr><th>Name:</th><th>Type:</th><th>doc:</th></td>\n";
		genDoc(&text, a.analyzer()->builtinMethods());
		text << "</table>\n";
		text << "</body>\n</html>";
	}
	
	if(args->isSet("list")) {
		QStringList checks = AnalitzaCheck::listChecks();
		foreach(const QString& check, checks) {
			qDebug() << "- " << qPrintable(check);
		}
		if(checks.isEmpty())
			qDebug() << "None found";
	}
	return 0;
}
